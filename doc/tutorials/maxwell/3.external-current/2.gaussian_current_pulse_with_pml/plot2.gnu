set output 'tutorial_03.2-plot_current_td.png
set term png size 500,400
set xlabel 'time step'
set ylabel 'current density J_z'
p 'current_vs_t.dat' w l

