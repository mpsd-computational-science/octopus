#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt *.eps
mpirun -n 8 octopus >& log


cp tutorial.sh $OCTOPUS_TOP/doc/tutorials/other/magnons/1.gs/
