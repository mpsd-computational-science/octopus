set xlabel "x (a.u.)"
set t postscript enhanced color font "Monospace-Bold,25" landscape size 11,8.5
set output "GS_wavefunctions.eps"
set key top right

set rmargin 4.5
set lmargin 10.5
set tmargin 3.2
set bmargin 5.5


plot [-6:6][-1.2:0.8] \
    'static/wf-st00001.y=0,z=0' t 'n = 1' w l lw 2 linecolor rgb "red", \
    'static/wf-st00002.y=0,z=0' t 'n = 2' w l lw 2 linecolor rgb "green", \
    'static/v0.y=0,z=0' t "v(x)" w l lw 2  linecolor rgb "black"
