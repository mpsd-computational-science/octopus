*********************** SCF CYCLE ITER #   12 ************************
 etot  = -4.46666631E-01 abs_ev   =  1.78E-08 rel_ev   =  7.65E-08
 ediff =        5.24E-09 abs_dens =  7.04E-08 rel_dens =  7.04E-08
Matrix vector products:     11
Converged eigenvectors:      1

#  State  Eigenvalue [H]  Occupation    Error
      1       -0.233152    1.000000   ( 4.5E-08)

Elapsed time for SCF step    12:          0.01
**********************************************************************
