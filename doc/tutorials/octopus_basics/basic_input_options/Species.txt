****************************** Species *******************************
  Species 'N'
    type             : pseudopotential
    file             : '/home/luedersm/Octopus_foss_2022a_mpi_debug/share/octopus/pseudopotentials/PSF/N.psf'
    file format      : PSF
    valence charge   : 5.0
    atomic number    :   7
    form on file     : semilocal
    orbital origin   : calculated
    lmax             : 1
    llocal           : 0
    projectors per l : 1
    total projectors : 1
    application form : kleinman-bylander
    orbitals         : 16
    bound orbitals   : 16

**********************************************************************
