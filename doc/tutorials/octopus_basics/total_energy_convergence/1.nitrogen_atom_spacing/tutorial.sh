#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

cp $OCTOPUS_TOP/testsuite/tutorials/02-octopus_basics-basic_input_options.01-N_atom.inp inp

. ./spacing.sh

gnuplot plot.gp

cp *.eps tutorial.sh *.dat $OCTOPUS_TOP/doc/tutorials/octopus_basics/total_energy_convergence/1.nitrogen_atom_spacing/
