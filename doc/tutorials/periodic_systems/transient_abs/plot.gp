set t postscript enhanced color font "Monospace-Bold,25" landscape size 12,8.5
set output "Transient_abs.eps"
unset key

set xlabel "Delay (a.u.)"
set ylabel "Energy (eV)"
set cblabel "Optical conductivity"

set rmargin 4.5
set lmargin 10.5
set tmargin 3.2
set bmargin 5.5

set xzeroaxis
set xrange [0:1500]
set yrange [0:5]
set mxtics 5
set mytics 5

y = 3

set label "A(t)" at 100, 4.3 front tc rgb "white"

set view map
sp 'result.dat' u 1:($2*27.2114):3 w pm3d, \
   'td.general_ref/laser' u 2:($3/10+4.1):(0) w l lw 3 lc rgb "white" 

