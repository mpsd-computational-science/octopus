!! Copyright (C) 2021 N. Tancogne-Dejean
!!
!! This Source Code Form is subject to the terms of the Mozilla Public
!! License, v. 2.0. If a copy of the MPL was not distributed with this
!! file, You can obtain one at https://mozilla.org/MPL/2.0/.
!!

#include "global.h"

module density_interaction_oct_m
  use interaction_oct_m

  implicit none

  private
  public ::                &
    density_interaction_t

  type, extends(interaction_t), abstract :: density_interaction_t
    FLOAT, allocatable, public :: density(:,:)
  end type density_interaction_t

end module density_interaction_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
