target_sources(Octopus_lib PRIVATE
		poisson.F90
		poisson_cg.F90
		poisson_corrections.F90
		poisson_cutoff.F90
		poisson_cutoffs.c
		poisson_fft.F90
		poisson_fmm.F90
		poisson_isf.F90
		poisson_multigrid.F90
		poisson_no.F90
		poisson_psolver.F90
		scaling_function.F90
		)
if (TARGET PSolver::PSolver)
	target_link_libraries(Octopus_lib PRIVATE PSolver::PSolver)
endif ()
