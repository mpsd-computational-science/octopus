!! Copyright (C) 2008 X. Andrade
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

!> @brief This module defines stencils used in Octopus
!!
!! Stencils are a fundamental concept in finite difference methods.
!! In general, semi-local operations, such as derivatives, can be extressed
!! as a weighed sum over a neighbourhood of points, relative to the point
!! for which the operation is performed.
!!
!! The set of points which are used in this sum is called the stencil.
!! Stencils can differ by their shape (e.g. a n-dimensional cross, a star, etc.)
!! and their order, i.e. the number of points in a given direction.
!!
!! Various specific stencils are defined in the modules:
!! - stencil_cube_oct_m
!! - stencil_star_oct_m
!! - stencil_stargeneral_oct_m
!! - stencil_starplus_oct_m
!! - stencil_variational_oct_m
!!
!! These differ in the ways the stencils are constructed, but are all represented by stencil_oct_m::stencil_t.
!!

module stencil_oct_m
  use debug_oct_m
  use global_oct_m
  use messages_oct_m
  use profiling_oct_m

  implicit none

  private
  public ::                        &
    stencil_t,                     &
    stencil_allocate,              &
    stencil_copy,                  &
    stencil_end,                   &
    stencil_init_center,           &
    stencil_union

  type stargeneral_arms_t
    ! Components are public by default
    integer          :: arms(1:3,1:3) = reshape([0,0,0,0,0,0,0,0,0], (/3, 3/))
    integer          :: narms = 0
  end type stargeneral_arms_t

  !> @brief The class representing the stencil, which is used for non-local mesh operations.
  !!
  !! The points of the stencil are stored in terms of the 'spatial index'
  !! (see mesh_init_oct_m::mesh_init_stage_2()).

  type stencil_t
    ! Components are public by default
    integer, private     :: dim           !< spatial dimension
    integer              :: center        !< index to the central point of the stencil
    integer              :: size          !< size (number of points)
    integer              :: npoly         !< this is modified in derivatives_oct_m::derivatives_build()
    integer, allocatable :: points(:, :)  !< list of points in spatial index notation (1:dim, 1:size)

    ! The stargeneral arms
    type(stargeneral_arms_t) :: stargeneral
  end type stencil_t

contains

  !-------------------------------------------------------
  subroutine stencil_allocate(this, dim, size)
    type(stencil_t), intent(inout) :: this
    integer,         intent(in)    :: dim
    integer,         intent(in)    :: size

    PUSH_SUB(stencil_allocate)

    this%dim = dim
    this%size = size
    this%npoly = size

    SAFE_ALLOCATE(this%points(1:this%dim, 1:size))

    this%points = 0

    POP_SUB(stencil_allocate)
  end subroutine stencil_allocate

  !-------------------------------------------------------
  subroutine stencil_copy(input, output)
    type(stencil_t), intent(in)  :: input
    type(stencil_t), intent(out) :: output

    PUSH_SUB(stencil_copy)

    call stencil_allocate(output, input%dim, input%size)
    output%points = input%points
    output%center = input%center
    output%npoly = input%npoly

    output%stargeneral%narms = input%stargeneral%narms
    output%stargeneral%arms = input%stargeneral%arms

    POP_SUB(stencil_copy)
  end subroutine stencil_copy


  !-------------------------------------------------------
  subroutine stencil_end(this)
    type(stencil_t), intent(inout) :: this

    PUSH_SUB(stencil_end)

    SAFE_DEALLOCATE_A(this%points)

    POP_SUB(stencil_end)
  end subroutine stencil_end


  !-------------------------------------------------------
  subroutine stencil_union(st1, st2, stu)
    type(stencil_t), intent(inout) :: st1
    type(stencil_t), intent(inout) :: st2
    type(stencil_t), intent(inout) :: stu

    integer :: ii, jj, nstu
    logical :: not_in_st1

    PUSH_SUB(stencil_union)

    ASSERT(st1%dim == st2%dim)

    call stencil_allocate(stu, st1%dim, st1%size + st2%size)

    ! copy the first stencil
    do ii = 1, st1%size
      stu%points(:, ii) = st1%points(:, ii)
    end do

    nstu = st1%size

    do ii = 1, st2%size

      not_in_st1 = .true.

      ! check whether that point was already in the stencil
      do jj = 1, st1%size
        if (all(st1%points(:, jj) == st2%points(:, ii))) then
          not_in_st1 = .false.
          exit
        end if
      end do

      if (not_in_st1) then !add it
        nstu = nstu + 1
        stu%points(:, nstu) = st2%points(:, ii)
      end if

    end do

    stu%size = nstu

    ! this is not defined for a union, which could be any combination. The
    ! weights should already be known here
    stu%npoly = -1

    call stencil_init_center(stu)

    POP_SUB(stencil_union)
  end subroutine stencil_union


  !-------------------------------------------------------
  subroutine stencil_init_center(this)
    type(stencil_t), intent(inout) :: this

    integer :: ii

    PUSH_SUB(stencil_init_center)

    this%center = -1

    do ii = 1, this%size
      if (all(this%points(:, ii) == 0)) this%center = ii
    end do

    POP_SUB(stencil_init_center)
  end subroutine stencil_init_center

end module stencil_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
