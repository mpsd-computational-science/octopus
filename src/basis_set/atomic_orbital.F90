!! Copyright (C) 2016 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.

#include "global.h"

module atomic_orbital_oct_m
  use allelectron_oct_m
  use box_cylinder_oct_m
  use box_minimum_oct_m
  use box_sphere_oct_m
  use debug_oct_m
  use global_oct_m
  use, intrinsic :: iso_fortran_env
  use jellium_oct_m
  use lalg_basic_oct_m
  use lattice_vectors_oct_m
  use loct_math_oct_m
  use math_oct_m
  use mesh_oct_m
  use messages_oct_m
  use namespace_oct_m
  use orbitalset_oct_m
  use profiling_oct_m
  use ps_oct_m
  use pseudopotential_oct_m
  use space_oct_m
  use species_oct_m
  use species_factory_oct_m
  use splines_oct_m
  use submesh_oct_m

  implicit none

  private

  public ::                           &
    atomic_orbital_get_radius,        &
    datomic_orbital_get_submesh,      &
    zatomic_orbital_get_submesh,      &
    datomic_orbital_get_submesh_safe, &
    zatomic_orbital_get_submesh_safe, &
    dget_atomic_orbital,              &
    zget_atomic_orbital,              &
    l_notation

  character(len=1), parameter :: &
    l_notation(0:3) = (/ 's', 'p', 'd', 'f' /)

  integer, public, parameter :: MAX_L = 4

contains

  ! ---------------------------------------------------------
  FLOAT function atomic_orbital_get_radius(species, mesh, iorb, ispin, truncation, threshold) result(radius)
    class(species_t),         intent(in)   :: species
    type(mesh_t),             intent(in)   :: mesh
    integer,                  intent(in)   :: iorb, ispin
    integer(int64),              intent(in)   :: truncation
    FLOAT,                    intent(in)   :: threshold

    integer :: ii, ll, mm

    PUSH_SUB(atomic_orbital_get_radius)

    call species%get_iwf_ilm(iorb, ispin, ii, ll, mm)

    if (truncation == OPTION__AOTRUNCATION__AO_FULL) then
      radius = species%get_iwf_radius(ii, ispin, threshold)
    else
      radius = species%get_iwf_radius(ii, ispin)

      if (truncation == OPTION__AOTRUNCATION__AO_BOX) then
        ! if the orbital is larger than the size of the box, we restrict it to this size,
        ! otherwise the orbital will overlap more than one time with the simulation box.
        ! This would induces phase problem if the complete mesh is used instead of the sphere
        radius = min(radius, minval(mesh%box%bounding_box_l(1:mesh%box%dim)-mesh%spacing(1:mesh%box%dim)*1.01_real64))
      else
        !If asked, we truncate the orbital to the radius on the projector spheres
        !of the NL part of the pseudopotential.
        !This is a way to garanty no overlap between orbitals of different atoms.
        select type(species)
        type is(pseudopotential_t)
          radius = min(radius, species%get_radius())
        end select
      end if

    end if
    ! make sure that if the spacing is too large, the orbitals fit in a few points at least
    radius = max(radius, M_TWO*maxval(mesh%spacing))

    POP_SUB(atomic_orbital_get_radius)
  end function atomic_orbital_get_radius


#include "undef.F90"
#include "real.F90"
#include "atomic_orbital_inc.F90"

#include "undef.F90"
#include "complex.F90"
#include "atomic_orbital_inc.F90"

end module atomic_orbital_oct_m
