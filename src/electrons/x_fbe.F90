!! Copyright (C) 2002-2006 M. Marques, A. Castro, A. Rubio, G. Bertsch
!! Copyright (C) 2023 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module x_fbe_oct_m
  use comm_oct_m
  use debug_oct_m
  use derivatives_oct_m
  use electron_space_oct_m
  use exchange_operator_oct_m
  use global_oct_m
  use grid_oct_m
  use kpoints_oct_m
  use messages_oct_m
  use mesh_oct_m
  use mesh_function_oct_m
  use mpi_oct_m
  use namespace_oct_m
  use profiling_oct_m
  use poisson_oct_m
  use ring_pattern_oct_m
  use space_oct_m
  use states_abst_oct_m
  use states_elec_oct_m
  use states_elec_dim_oct_m

  implicit none

  private
  public ::               &
    x_fbe_calc

contains

  !>@brief Interface to X(x_fbe_calc)
  subroutine x_fbe_calc (namespace, psolver, gr, der, st, ex, vxc)
    type(namespace_t),           intent(in)    :: namespace
    type(poisson_t),             intent(in)    :: psolver
    type(grid_t),                intent(in)    :: gr
    type(derivatives_t),         intent(in)    :: der
    type(states_elec_t),         intent(inout) :: st
    FLOAT,                       intent(inout) :: ex
    FLOAT, optional,             intent(inout) :: vxc(:,:) !< vxc(gr%mesh%np, st%d%nspin)

    PUSH_SUB(x_fbe_calc)

    if (states_are_real(st)) then
      call dx_fbe_calc(namespace, psolver, gr, der, st, ex, vxc)
    else
      call zx_fbe_calc(namespace, psolver, gr, der, st, ex, vxc)
    end if

    POP_SUB(x_fbe_calc)
  end subroutine x_fbe_calc

#include "undef.F90"
#include "real.F90"
#include "x_fbe_inc.F90"

#include "undef.F90"
#include "complex.F90"
#include "x_fbe_inc.F90"

end module x_fbe_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
