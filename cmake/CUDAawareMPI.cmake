list(APPEND CMAKE_MESSAGE_CONTEXT CUDAawareMPI)
find_package(MPI QUIET COMPONENTS C)
try_compile(cuda_aware_mpi_compile ${CMAKE_CURRENT_BINARY_DIR}/CMakeFiles/CMakeTmp
	SOURCES ${CMAKE_CURRENT_LIST_DIR}/test_cuda_aware_mpi.c
	LINK_LIBRARIES MPI::MPI_C
)
if (cuda_aware_mpi_compile)
	set(HAVE_CUDA_MPI 1)
else ()
	message(STATUS "Failed to compile test_cuda_aware_mpi.c")
endif ()
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
