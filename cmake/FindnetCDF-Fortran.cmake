#[==============================================================================================[
#                                 netCDF compatibility wrapper                                 #
]==============================================================================================]

#[===[.md
# FindnetCDF-Fortran

netCDF compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET netCDF::Fortran)
	return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT FindnetCDF-Fortran)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
		NAMES netCDF-Fortran
		PKG_MODULE_NAMES netcdf-fortran)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
	add_library(netCDF::Fortran ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
elseif (NOT TARGET netCDF::Fortran)
	# Upstream has a horrible naming scheme :(
	# https://github.com/Unidata/netcdf-fortran/issues/404
	if (TARGET netCDF::netcdff)
		add_library(netCDF::Fortran ALIAS netCDF::netcdff)
	elseif (TARGET netcdff)
		add_library(netCDF::Fortran ALIAS netcdff)
	endif ()
endif ()
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND)
	set(HAVE_NETCDF 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
		URL https://github.com/Unidata/netcdf-fortran
		DESCRIPTION "The Unidata network Common Data Form (netCDF) is an interface for scientific data access and a freely-distributed software library that provides an implementation of the interface"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
