# Note: Some of these metadata are found in Find<Package>.cmake and the FetchContent<Package>_After.cmake files
## Build system
set_package_properties(PkgConfig PROPERTIES
		URL https://github.com/pkgconf/pkgconf
		DESCRIPTION "Package compiler and linker metadata toolkit"
		TYPE REQUIRED
		PURPOSE "Build system"
)

## Hardware
set_package_properties(CUDA PROPERTIES
		URL https://developer.nvidia.com/cuda-zone
		DESCRIPTION "CUDA® is a parallel computing platform and programming model developed by NVIDIA for general computing on graphical processing units (GPUs)"
		TYPE OPTIONAL
		PURPOSE "Graphics card support"
)
set_package_properties(OpenCL PROPERTIES
		URL https://registry.khronos.org/OpenCL/
		DESCRIPTION "OpenCL (Open Computing Language) is a framework for writing programs that execute across heterogeneous platforms"
		TYPE OPTIONAL
		PURPOSE "Graphics card support"
)

## Parallelization
set_package_properties(MPI PROPERTIES
		URL https://cmake.org/cmake/help/latest/module/FindMPI.html
		DESCRIPTION "Message Passing Interface (MPI) vendor (OpenMPI/MPICH/IntelMPI)"
		TYPE RECOMMENDED
		PURPOSE "Parallelization library"
)
set_package_properties(OpenMP PROPERTIES
		URL https://www.openmp.org/resources/openmp-compilers-tools/
		DESCRIPTION "The OpenMP API specification for parallel programming"
		TYPE RECOMMENDED
		PURPOSE "Parallelization library"
)

## Numerical libraries
set_package_properties(GSL PROPERTIES
		URL https://www.gnu.org/software/gsl/
		DESCRIPTION "The GNU Scientific Library (GSL) is a numerical library for C and C++ programmers"
		TYPE REQUIRED
		PURPOSE "Numerical library"
)
set_package_properties(MKL PROPERTIES
		URL https://www.intel.com/content/www/us/en/developer/tools/oneapi/onemkl.html
		DESCRIPTION "Highly optimized, fast, and complete library of math functions for Intel® CPUs and GPUs"
		TYPE RECOMMENDED
		PURPOSE "Numerical library"
)
set_package_properties(FFTW PROPERTIES
		URL https://github.com/FFTW/fftw3/
		DESCRIPTION "FFTW is a C subroutine library for computing the discrete Fourier transform (DFT)"
		TYPE OPTIONAL
		PURPOSE "Numerical library"
)
set_package_properties(FFTW PROPERTIES
		TYPE OPTIONAL
		PURPOSE "FFT library"
)
set_package_properties(BLAS PROPERTIES
		URL https://www.netlib.org/blas/
		DESCRIPTION "The BLAS (Basic Linear Algebra Subprograms) are routines that provide standard building blocks for performing basic vector and matrix operations"
		TYPE OPTIONAL
		PURPOSE "Numerical library"
)
set_package_properties(LAPACK PROPERTIES
		URL https://www.netlib.org/lapack/
		DESCRIPTION "LAPACK is written in Fortran 90 and provides routines for solving systems of simultaneous linear equations, least-squares solutions of linear systems of equations, eigenvalue problems, and singular value problems"
		TYPE OPTIONAL
		PURPOSE "Numerical library"
)
set_package_properties(SCALAPACK PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Numerical library"
)
set_package_properties(ELPA PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Numerical library"
)
set_package_properties(NLopt PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Numerical library"
)
set_package_properties(nfft PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Numerical library"
)
set_package_properties(nfft PROPERTIES
		TYPE OPTIONAL
		PURPOSE "FFT library"
)
set_package_properties(pfft PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Numerical library"
)
set_package_properties(pfft PROPERTIES
		TYPE OPTIONAL
		PURPOSE "FFT library"
)
set_package_properties(pnfft PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Numerical library"
)
set_package_properties(pnfft PROPERTIES
		TYPE OPTIONAL
		PURPOSE "FFT library"
)
set_package_properties(CLBlast PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Numerical library"
)
set_package_properties(clFFT PROPERTIES
		TYPE OPTIONAL
		PURPOSE "FFT library"
)
set_package_properties(PSolver PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Numerical library"
)
set_package_properties(SPARSKIT PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Numerical library"
)

## Scientific packages
set_package_properties(Libxc PROPERTIES
		TYPE REQUIRED
		PURPOSE "Scientific library"
)
set_package_properties(Spglib PROPERTIES
		TYPE REQUIRED
		PURPOSE "Scientific library"
)
set_package_properties(DftbPlus PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Scientific library"
)
set_package_properties(libvdwxc PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Scientific library"
)

## I/O
set_package_properties(netCDF-Fortran PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Octopus output format"
)
set_package_properties(BerkleyGW PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Octopus output format"
)
set_package_properties(etsf-io PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Octopus output format"
)


## Other
set_package_properties(GD PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Other?"
)
set_package_properties(CGAL PROPERTIES
		URL https://github.com/CGAL/cgal
		DESCRIPTION "C++ library that aims to provide easy access to efficient and reliable algorithms in computational geometry."
		TYPE OPTIONAL
		PURPOSE "Geometry library"
)
set_package_properties(METIS PROPERTIES
		TYPE REQUIRED
		PURPOSE "Geometry library"
)
set_package_properties(ParMETIS PROPERTIES
		TYPE OPTIONAL
		PURPOSE "Geometry library"
)
