#[==============================================================================================[
#                                BerkleyGW compatibility wrapper                                #
]==============================================================================================]

#[===[.md
# FindBerkleyGW

BerkleyGW compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET BerkleyGW::berkleygw)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT FindBerkleyGW)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        HAVE_FALLBACK
        NAMES BerkleyGW
        PKG_MODULE_NAMES berkleygw)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(BerkleyGW::berkleygw ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()
# Temporary workarounds for non-packages
if (NOT ${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    find_library(BerkleyGW_LIBRARY
            NAMES BGW_wfn
    )
    mark_as_advanced(BerkleyGW_LIBRARY)
    find_path(BerkleyGW_INCLUDE_DIR
            NAMES wfn_rho_vxc_io_m.mod
            PATH_SUFFIXES berkleygw BerkleyGW
    )
    mark_as_advanced(BerkleyGW_INCLUDE_DIR)
    find_package_handle_standard_args(${CMAKE_FIND_PACKAGE_NAME}
            REQUIRED_VARS BerkleyGW_LIBRARY BerkleyGW_INCLUDE_DIR
    )
    if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
        set(${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS ${BerkleyGW_INCLUDE_DIR})
        set(${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES ${BerkleyGW_LIBRARY})
        add_library(BerkleyGW::berkleygw UNKNOWN IMPORTED)
        set_target_properties(BerkleyGW::berkleygw PROPERTIES
                IMPORTED_LOCATION ${BerkleyGW_LIBRARY}
                IMPORTED_LINK_INTERFACE_LANGUAGES Fortran
        )
        target_include_directories(BerkleyGW::berkleygw INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS})
        target_link_libraries(BerkleyGW::berkleygw INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES})
    endif()
endif ()
if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    set(HAVE_BERKELEYGW 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://berkeleygw.org/
        DESCRIPTION "BerkeleyGW is a many-body perturbation theory code for excited states, using the GW method and the GW plus Bethe-Salpeter equation (GW-BSE)"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
