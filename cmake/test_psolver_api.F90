program test_psolver_api
    use Poisson_Solver
    use at_domain
    use dictionaries
    implicit none

    ! Declare variables
    integer :: iproc, nproc
    type(dictionary),pointer :: dict
    type(domain) :: dom
    integer, dimension(3) :: ndims
    real(gp), dimension(3) :: hgrids
    type(coulomb_operator) :: kernel
    type(coulomb_operator) :: karray

    ! Initialize  mandatory variables
    nullify(dict)

    ! Call the function
    karray = pkernel_init(iproc, nproc, dict, dom, ndims, hgrids)

end program test_psolver_api
