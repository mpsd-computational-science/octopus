#[==============================================================================================[
#                                 PSolver compatibility wrapper                                 #
]==============================================================================================]

#[===[.md
# FindPSolver

PSolver compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET PSolver::PSolver)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT FindPSolver)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        NAMES PSolver
        PKG_MODULE_NAMES psolver)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(PSolver::PSolver ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()
if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    set(HAVE_PSOLVER 1)
    # Bug in LINK_LIBRARIES and aliased target. Need to get the real target
    # https://gitlab.kitware.com/cmake/cmake/-/issues/25337
    get_target_property(_real_target PSolver::PSolver ALIASED_TARGET)
    if (NOT _real_target)
        set(_real_target PSolver::PSolver)
    endif ()
    # PSolver's pkgconfig is broken, need to try various other variants
    set(try_link_libraries "${_real_target}")
    find_package(atlab QUIET)
    if (atlab_FOUND)
        get_target_property(_real_atlab_target atlab::atlab ALIASED_TARGET)
        if (NOT _real_atlab_target)
            set(_real_target atlab::atlab)
        endif ()
        list(APPEND try_link_libraries "${_real_target}\;${_real_atlab_target}")
    endif ()
    find_package(OpenMP QUIET COMPONENTS Fortran)
    if (OpenMP_Fortran_FOUND)
        if (atlab_FOUND)
            list(APPEND try_link_libraries "${_real_target}\;${_real_atlab_target}\;OpenMP::OpenMP_Fortran")
        else ()
            list(APPEND try_link_libraries "${_real_target}\;OpenMP::OpenMP_Fortran")
        endif ()
    endif ()
    find_package(MPI QUIET COMPONENTS Fortran)
    if (MPI_Fortran_FOUND)
        if (OpenMP_Fortran_FOUND)
            if (atlab_FOUND)
                list(APPEND try_link_libraries "${_real_target}\;${_real_atlab_target}\;OpenMP::OpenMP_Fortran\;MPI::MPI_Fortran")
            else ()
                list(APPEND try_link_libraries "${_real_target}\;OpenMP::OpenMP_Fortran\;MPI::MPI_Fortran")
            endif ()
        else ()
            if (atlab_FOUND)
                list(APPEND try_link_libraries "${_real_target}\;${_real_atlab_target};MPI::MPI_Fortran")
            else ()
                list(APPEND try_link_libraries "${_real_target}\;MPI::MPI_Fortran")
            endif ()
        endif ()
    endif ()
    foreach (link_libraries IN ITEMS ${try_link_libraries})
        if (CMAKE_VERSION LESS 3.25)
            try_compile(PSolver_new_api ${CMAKE_CURRENT_BINARY_DIR}/CMakeFiles/CMakeTmp
                    SOURCES ${CMAKE_CURRENT_LIST_DIR}/test_psolver_api.F90
                    LINK_LIBRARIES ${link_libraries}
            )
        else ()
            # TODO: Move to SOURCE_FROM_VAR when moving to 3.25
            try_compile(PSolver_new_api
                    SOURCES ${CMAKE_CURRENT_LIST_DIR}/test_psolver_api.F90
                    LINK_LIBRARIES ${link_libraries}
                    NO_CACHE
            )
        endif ()
        if (PSolver_new_api)
            list(REMOVE_ITEM link_libraries "${_real_target}")
            # Add missing interface targets
            target_link_libraries(${_real_target} INTERFACE ${link_libraries})
        endif ()
    endforeach ()
    if (PSolver_new_api)
        set(HAVE_PSOLVER_NEW_API 1)
    endif ()
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://gitlab.com/l_sim/psolver
        DESCRIPTION "Poisson Solver is part of the BigDFT project"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
