#[==============================================================================================[
#                                  pnfft compatibility wrapper                                  #
]==============================================================================================]

#[===[.md
# Findpnfft

pnfft compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET pnfft::pnfft)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT Findpnfft)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        HAVE_FALLBACK
        NAMES pnfft
        PKG_MODULE_NAMES pnfft)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(pnfft::pnfft ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()
# Temporary workarounds for non-packages
if (NOT ${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    find_package(pfft MODULE)
    find_library(pnfft_LIBRARY
            NAMES pnfft
    )
    mark_as_advanced(pnfft_LIBRARY)
    find_path(pnfft_INCLUDE_DIR
            NAMES pnfft.f03
            PATH_SUFFIXES pnfft
    )
    mark_as_advanced(pnfft_INCLUDE_DIR)
    find_package_handle_standard_args(${CMAKE_FIND_PACKAGE_NAME}
            REQUIRED_VARS pnfft_LIBRARY pnfft_INCLUDE_DIR pfft_FOUND
    )
    if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
        set(${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS ${pnfft_INCLUDE_DIR})
        set(${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES ${pnfft_LIBRARY})
        add_library(pnfft::pnfft UNKNOWN IMPORTED)
        set_target_properties(pnfft::pnfft PROPERTIES
                IMPORTED_LOCATION ${pnfft_LIBRARY}
        )
        target_include_directories(pnfft::pnfft INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS})
        target_link_libraries(pnfft::pnfft INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES} pfft::pfft)
    endif()
endif ()
if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    set(HAVE_PNFFT 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://github.com/mpip/pnfft
        DESCRIPTION "PNFFT is a software library written in C for computing parallel nonequispaced fast Fourier transformations"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
