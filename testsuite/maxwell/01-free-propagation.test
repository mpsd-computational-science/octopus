# -*- coding: utf-8 mode: shell-script -*-

Test       : Free Maxwell propagation
Program    : octopus
TestGroups : short-run, maxwell
Enabled    : Yes

#This test checks the time propagation, for various propagators

# one cosinoidal pulse
Input      : 01-free-propagation.01-1_pulse_td.inp
Precision: 1.00e-04
match ;   Tot. Maxwell energy [step   0]   ; LINEFIELD(Maxwell/td.general/maxwell_energy, -100, 3) ; 0.0
Precision: 3.49e-15
match ;   Tot. Maxwell energy [step 100]   ; LINEFIELD(Maxwell/td.general/maxwell_energy, -1, 3) ; 0.3486989733299418

# Poynting-vector-x plane_z
Precision: 8.17e-17
match ;  Poynting x  (z=0) [step 50]  ; LINEFIELD(Maxwell/output_iter/td.0000050/poynting_vector-x.z\=0, 121, 3) ; 0.00816900125629657
Precision: 1.00e-04
match ;  Poynting x  (z=0) [step 100]  ; LINEFIELD(Maxwell/output_iter/td.0000100/poynting_vector-x.z\=0, 121, 3) ; 0.0

# Orbital Angular Momentum-z plane_z
Precision: 1.13e-22
match ;   orbital angular momentum z  (z=0) [step 50]   ; LINEFIELD(Maxwell/output_iter/td.0000050/orbital_angular_momentum-z.z\=0, 741, 3) ; -2.2649157806232002e-10
Precision: 7.20e-16
match ;   orbital angular momentum z  (z=0) [step 100]   ; LINEFIELD(Maxwell/output_iter/td.0000100/orbital_angular_momentum-z.z\=0, 741, 3) ; -0.034838668355840305

# Energy density
Precision: 6.2e-19
match ;  energy density (x=-10,y=0,z=0) [step 100]  ; LINEFIELD(Maxwell/output_iter/td.0000100/maxwell_energy_density\.y=0\,z\=0, 24, 2) ; 1.1288166880731001e-05
Precision: 1.59e-20
match ;  energy density (x=25,y=0,z=0) [step 100]  ; LINEFIELD(Maxwell/output_iter/td.0000100/maxwell_energy_density\.y=0\,z\=0, 31, 2) ; 3.17908110834685e-07

Precision: 1.59e-16
match ;   Ez (z=0) [step 80]   ; LINEFIELD(Maxwell/td.general/total_e_field_z, 87, 3) ; -0.01590299677520406
Precision: 1.58e-15
match ;   Ez (z=-5) [step 80]   ; LINEFIELD(Maxwell/td.general/total_e_field_z, 87, 4) ; -1.5833771027805373e-02
match ;   Ez (z=5) [step 80]   ; LINEFIELD(Maxwell/td.general/total_e_field_z, 87, 5) ; -1.5833771027805376e-02
Precision: 1.16e-18
match ;   By (z=0) [step 80]   ; LINEFIELD(Maxwell/td.general/total_b_field_y, 87, 3) ; 0.000116049773872968
Precision: 1.15e-17
match ;   By (z=-5) [step 80]   ; LINEFIELD(Maxwell/td.general/total_b_field_y, 87, 4) ; 1.1553637382870730e-04
match ;   By (z=5) [step 80]   ; LINEFIELD(Maxwell/td.general/total_b_field_y, 87, 5) ; 1.1553637382870731e-04

# two cosinoidal pulses
Input      : 01-free-propagation.02-2_pulses_td.inp
Precision: 1.46e-16
match ;     Tot. Maxwell energy [step 10]     ; LINEFIELD(Maxwell/td.general/maxwell_energy, 16, 3) ; 0.01464910419570856
Precision: 1.18e-13
match ;     Tot. Maxwell energy [step 30]     ; LINEFIELD(Maxwell/td.general/maxwell_energy, 36, 3) ; 11.84850858797902
Precision: 1.0e-18
match ;     Ex  (x=-10,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-x\.y=0\,z=0, 12, 2) ; 0.0
Precision: 8.0e-18
match ;     Ex  (x=-10,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.y=0\,z=0, 12, 2) ; 0.0
Precision: 2.0e-18
match ;     Ex  (x=  0,y=-10,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-x\.x=0\,z=0, 12, 2) ; 0.0
Precision: 6.0e-18
match ;     Ex  (x=  0,y=-10,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.x=0\,z=0, 12, 2) ; 0.0
Precision: 2.0e-20
match ;     Ex  (x=  0,y=  0,z=-10) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-x\.x=0\,y=0, 12, 2) ; -7.44935874836001e-09
Precision: 9.0e-18
match ;     Ex  (x=  0,y=  0,z=-10) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-x\.x=0\,y=0, 12, 2) ; 3.92743564608993e-05
Precision: 1.00e-18
match ;     Ey  (x=-10,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-y\.y=0\,z=0, 12, 2) ; 0.0
Precision: 8.0e-18
match ;     Ey  (x=-10,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-y\.y=0\,z=0, 12, 2) ; 0.0
Precision: 2.0e-18
match ;     Ey  (x=  0,y=-10,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-y\.x=0\,z=0, 12, 2) ; 0.0
Precision: 8.0e-18
match ;     Ey  (x=  0,y=-10,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-y\.x=0\,z=0, 12, 2) ; 0.0
Precision: 1.81e-22
match ;     Ey  (x=  0,y=  0,z=-10) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-y\.x=0\,y=0, 12, 2) ; -3.6139804519503e-09
Precision: 3.0e-17
match ;     Ey  (x=  0,y=  0,z=-10) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-y\.x=0\,y=0, 12, 2) ; 6.21420544338422e-05
Precision: 1.00e-09
match ;     Ez  (x=-10,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.y=0\,z=0, 12, 2) ; 2.003e-07
Precision: 3.40e-16
match ;     Ez  (x=-10,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.y=0\,z=0, 12, 2) ; 0.00679890411328333
Precision: 2.1e-21
match ;     Ez  (x=  0,y=-10,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,z=0, 12, 2) ; 1.70505665461285e-07
Precision: 4.94e-17
match ;     Ez  (x=  0,y=-10,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.x=0\,z=0, 12, 2) ; -0.0038674015439683243
Precision: 4.28e-20
match ;     Ez  (x=  0,y=  0,z=-10) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/e_field-z\.x=0\,y=0, 12, 2) ; -8.559912092005039e-07
Precision: 6.77e-16
match ;     Ez  (x=  0,y=  0,z=-10) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/e_field-z\.x=0\,y=0, 12, 2) ; -0.06768311504600791
Precision: 5.31e-23
match ;     Bx  (x=-10,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.y=0\,z=0, 12, 2) ; 1.0625740169348701e-09
Precision: 1.78e-18
match ;     Bx  (x=-10,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-x\.y=0\,z=0, 12, 2) ; -3.5599442995487606e-05
Precision: 5.17e-22
match ;     Bx  (x=  0,y=-10,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.x=0\,z=0, 12, 2) ; 1.0337812463732e-09
Precision: 1.54e-19
match ;     Bx  (x=  0,y=-10,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-x\.x=0\,z=0, 12, 2) ; 5.21435619650283e-06
Precision: 2.58e-22
match ;     Bx  (x=  0,y=  0,z=-10) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-x\.x=0\,y=0, 12, 2) ; -5.153470741611251e-09
Precision: 2.30e-18
match ;     Bx  (x=  0,y=  0,z=-10) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-x\.x=0\,y=0, 12, 2) ; 0.0002297756962735344
Precision: 1.0e-22
match ;     By  (x=-10,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.y=0\,z=0, 12, 2) ; -1.069343298653385e-09
Precision: 2.0e-19
match ;     By  (x=-10,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.y=0\,z=0, 12, 2) ; -6.303306459036279e-07
Precision: 3.75e-23
match ;     By  (x=  0,y=-10,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,z=0, 12, 2) ; -7.50397520665512e-10
Precision: 3.30e-19
match ;     By  (x=  0,y=-10,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.x=0\,z=0, 12, 2) ; -1.7667033944060303e-05
Precision: 2.10e-22
match ;     By  (x=  0,y=  0,z=-10) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-y\.x=0\,y=0, 12, 2) ; 4.2049658121720505e-09
Precision: 2.18e-17
match ;     By  (x=  0,y=  0,z=-10) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-y\.x=0\,y=0, 12, 2) ; -0.00043649799087836503
Precision: 1.0e-18
match ;     Bz  (x=-10,y=  0,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-z\.y=0\,z=0, 12, 2) ; 0.0
Precision: 1.0e-18
match ;     Bz  (x=-10,y=  0,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-z\.y=0\,z=0, 12, 2) ; 0.0
Precision: 1.0e-18
match ;     Bz  (x=  0,y=-10,z=  0) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-z\.x=0\,z=0, 12, 2) ; 0.0
Precision: 1.0e-18
match ;     Bz  (x=  0,y=-10,z=  0) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-z\.x=0\,z=0, 12, 2) ; 0.0
Precision: 1.03e-24
match ;     Bz  (x=  0,y=  0,z=-10) [step 10]     ; LINEFIELD(Maxwell/output_iter/td.0000010/b_field-z\.x=0\,y=0, 12, 2) ; 2.05208556494899e-11
Precision: 7.5e-20
match ;     Bz  (x=  0,y=  0,z=-10) [step 30]     ; LINEFIELD(Maxwell/output_iter/td.0000030/b_field-z\.x=0\,y=0, 12, 2) ; 2.16450527349468e-07
