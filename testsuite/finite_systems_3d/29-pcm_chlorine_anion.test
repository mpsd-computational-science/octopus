# -*- coding: utf-8 mode: shell-script -*-

Test       : Polarizable Continuum Model (PCM)
Program    : octopus
TestGroups : long-run, finite_systems_3d
Enabled    : Yes
Processors : 4

Input      : 29-pcm_chlorine_anion.01-ground_state-n60.inp
match ; SCF convergence               ; GREPCOUNT(static/info, 'SCF converged') ; 1
Precision: 8.58e-06
match ;    eigenvalue [1]                   ; GREPFIELD(static/info, '1   --', 3) ; -17.163066
Precision: 2.79e-05
match ;    eigenvalue [2]                   ; GREPFIELD(static/info, '2   --', 3) ; -5.576105
Precision: 2.79e-05
match ;    eigenvalue [3]                   ; GREPFIELD(static/info, '3   --', 3) ; -5.571308
Precision: 2.79e-05
match ;    eigenvalue [4]                   ; GREPFIELD(static/info, '4   --', 3) ; -5.570442
Precision: 2.71e-13
match ;    electrons-solvent int. energy    ; GREPFIELD(static/info, 'E_e-solvent =', 3) ; -27.0746236
Precision: 1.19e-07
match ;    nuclei-solvent int. energy       ; GREPFIELD(static/info, 'E_n-solvent =', 3) ; 23.85921657
Precision: 1.61e-07
match ;    molecule-solvent int. energy     ; GREPFIELD(static/info, 'E_M-solvent =', 3) ; -3.2154070299999997
Precision: 3.92e-07
match ;    electronic pol. charge           ; GREPFIELD(pcm/pcm_info.out, '       9   ', 7) ; -7.8420102499999995
Precision: 3.45e-07
match ;    nuclear pol. charge              ; GREPFIELD(pcm/pcm_info.out, '       9   ', 9) ; 6.90997129

Input      : 29-pcm_chlorine_anion.02-td_prop-n60.inp
Precision: 3.5e-13
match ;     M-solvent int. energy @ t=0        ; GREPFIELD(td.general/energy, '       0', 12) ; -3.2154067878159545
Precision: 2.0-13
match ;     M-solvent int. energy @ t=5*dt     ; GREPFIELD(td.general/energy, '       5', 12) ; -3.2154067871128538

Input      : 29-pcm_chlorine_anion.03-ground_state-n60-poisson.inp
match ;  SCF convergence                ; GREPCOUNT(static/info, 'SCF converged') ; 1.0
Precision: 8.60e-06
match ;     eigenvalue [1]                    ; GREPFIELD(static/info, '1   --', 3) ; -17.201808
Precision: 2.81e-05
match ;     eigenvalue [2]                    ; GREPFIELD(static/info, '2   --', 3) ; -5.618011000000001
Precision: 2.81e-04
match ;     eigenvalue [3]                    ; GREPFIELD(static/info, '3   --', 3) ; -5.6129
Precision: 2.81e-05
match ;     eigenvalue [4]                    ; GREPFIELD(static/info, '4   --', 3) ; -5.612521
Precision: 1.35e-07
match ;     electrons-solvent int. energy     ; GREPFIELD(static/info, 'E_e-solvent =', 3) ; -27.07580917
Precision: 1.19e-07
match ;     nuclei-solvent int. energy        ; GREPFIELD(static/info, 'E_n-solvent =', 3) ; 23.86015047
Precision: 1.61e-06
match ;     molecule-solvent int. energy      ; GREPFIELD(static/info, 'E_M-solvent =', 3) ; -3.2156586999999996
Precision: 3.92e-07
match ;     electronic pol. charge            ; GREPFIELD(pcm/pcm_info.out, '       9   ', 7) ; -7.84199666
Precision: 3.45e-07
match ;     nuclear pol. charge               ; GREPFIELD(pcm/pcm_info.out, '       9   ', 9) ; 6.90997129

Input      : 29-pcm_chlorine_anion.04-ground_state-n240.inp
match ;  SCF convergence                ; GREPCOUNT(static/info, 'SCF converged') ; 1.0
Precision: 8.70e-06
match ;     eigenvalue [1]                    ; GREPFIELD(static/info, '1   --', 3) ; -17.393714
Precision: 2.92e-05
match ;     eigenvalue [2]                    ; GREPFIELD(static/info, '2   --', 3) ; -5.832661
Precision: 2.92e-05
match ;     eigenvalue [3]                    ; GREPFIELD(static/info, '3   --', 3) ; -5.830904
Precision: 2.92e-05
match ;     eigenvalue [4]                    ; GREPFIELD(static/info, '4   --', 3) ; -5.830096999999999
Precision: 1.35e-07
match ;     electrons-solvent int. energy     ; GREPFIELD(static/info, 'E_e-solvent =', 3) ; -27.047671989999998
Precision: 1.19e-06
match ;     nuclei-solvent int. energy        ; GREPFIELD(static/info, 'E_n-solvent =', 3) ; 23.837475599999998
Precision: 1.61e-06
match ;     molecule-solvent int. energy      ; GREPFIELD(static/info, 'E_M-solvent =', 3) ; -3.2101964
Precision: 3.92e-07
match ;     electronic pol. charge            ; GREPFIELD(pcm/pcm_info.out, '       9   ', 7) ; -7.8411532799999994
Precision: 3.45e-06
match ;     nuclear pol. charge               ; GREPFIELD(pcm/pcm_info.out, '       9   ', 9) ; 6.909527199999999
