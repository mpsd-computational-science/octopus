foreach (test IN ITEMS
		01-sg15-h
		02-cu2_hgh
		03-nlcc
		04-carbon_dojo_psml
		05-carbon_dojo_pbesol
		06-carbon_dojo_pbe
		07-carbon_dojo_lda
		08-carbon_fhi
		09-carbon_cpi
		10-helium_upf
		11-isotopes
		12-mix_and_match
		13-U235
		14-carbon_dojo_psp8
		15-calcium_psp8_sic
		16-platinum_psp8
		17-hgh_occupations
		18-Bi_pseudodojo_fr	
)
	Octopus_add_test(${test}
			TEST_NAME ${curr_path}/${test}
	)
endforeach ()
