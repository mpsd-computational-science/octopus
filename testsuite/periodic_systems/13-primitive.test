# -*- coding: utf-8 mode: shell-script -*-

Test       : Primitive unit cells
Program    : octopus
TestGroups : long-run, periodic_systems
Enabled    : Yes

Processors : 4

Input      : 13-primitive.01-diamond.inp

match ;  SCF convergence  ; GREPCOUNT(static/info, 'SCF converged') ; 1.0

Precision: 5.74e-08
match ;   Total energy         ; GREPFIELD(static/info, 'Total       =', 3) ; -11.47041275
Precision: 5.37e-08
match ;   Ion-ion energy       ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -10.73490075
Precision: 4.13e-07
match ;   Eigenvalues sum      ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -0.82587376
Precision: 5.00e+00
match ;   Hartree energy       ; GREPFIELD(static/info, 'Hartree     =', 3) ; 1.0
Precision: 1.55e-07
match ;   Exchange energy      ; GREPFIELD(static/info, 'Exchange    =', 3) ; -3.10106481
Precision: 2.23e-07
match ;   Correlation energy   ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.44501701
Precision: 4.31e-07
match ;   Kinetic energy       ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 8.62110661
Precision: 3.41e-07
match ;   External energy      ; GREPFIELD(static/info, 'External    =', 3) ; -6.81055121

Precision: 7.28e-01
match ;   dipole 2-5   ; LINEFIELD(static/ks_me_dipole.k1_x, 4, 4) ; 0.712788020488
Precision: 5.11e-01
match ;   dipole 4-7   ; LINEFIELD(static/ks_me_dipole.k1_x, 9, 3) ; 1.031163079966

Precision: 1.00e-04
match ;    k-point 50 (x)    ; GREPFIELD(static/info, '#k =      50', 7) ; 0.0
match ;    k-point 50 (y)    ; GREPFIELD(static/info, '#k =      50', 8) ; 0.0
match ;    k-point 50 (z)    ; GREPFIELD(static/info, '#k =      50', 9) ; 0.0
Precision: 2.78e-05
match ;    Eigenvalue  1     ; GREPFIELD(static/info, '#k =      50', 3, 1) ; -0.556692
Precision: 1.08e-04
match ;    Eigenvalue  2     ; GREPFIELD(static/info, '#k =      50', 3, 2) ; 0.21625
Precision: 1.08e-04
match ;    Eigenvalue  3     ; GREPFIELD(static/info, '#k =      50', 3, 3) ; 0.21625
Precision: 1.08e-05
match ;    Eigenvalue  4     ; GREPFIELD(static/info, '#k =      50', 3, 4) ; 0.216251

Precision: 2.78e-05
match ;    k-point 75 (x)    ; GREPFIELD(static/info, '#k =      75', 7) ; 0.05555600000000001
match ;    k-point 75 (y)    ; GREPFIELD(static/info, '#k =      75', 8) ; 0.05555600000000001
match ;    k-point 75 (z)    ; GREPFIELD(static/info, '#k =      75', 9) ; 0.05555600000000001
Precision: 2.77e-05
match ;    Eigenvalue  1    ; GREPFIELD(static/info, '#k =      75', 3, 1) ; -0.553558
Precision: 9.77e-05
match ;    Eigenvalue  2    ; GREPFIELD(static/info, '#k =      75', 3, 2) ; 0.19545
Precision: 1.05e-05
match ;    Eigenvalue  3    ; GREPFIELD(static/info, '#k =      75', 3, 3) ; 0.210745
Precision: 1.05e-05
match ;    Eigenvalue  4    ; GREPFIELD(static/info, '#k =      75', 3, 4) ; 0.210745

Input      : 13-primitive.02-graphene.inp
Precision: 1e-04
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1

Precision: 1.98e-07
match ;     Total energy           ; GREPFIELD(static/info, 'Total       =', 3) ; -11.45702067
Precision: 9.90e-08
match ;     Ion-ion energy         ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -19.800128519999998
Precision: 1.52e-07
match ;     Exchange energy        ; GREPFIELD(static/info, 'Exchange    =', 3) ; -3.03647926
Precision: 2.19e-07
match ;     Correlation energy     ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.43850078
Precision: 2.00e-07
match ;      Eigenvalues sum       ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -4.0077115800000005
Precision: 8.80e-08
match ;      Hartree energy        ; GREPFIELD(static/info, 'Hartree     =', 3) ; -11.28227461
Precision: 4.16e-06
match ;      Kinetic energy        ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 8.3172716
Precision: 7.39e-08
match ;      External energy       ; GREPFIELD(static/info, 'External    =', 3) ; 14.78308974

Precision: 1.00e-04
match ;     k-point 26 (x)     ; GREPFIELD(static/info, '#k =      26', 7) ; 0.0
match ;     k-point 26 (y)     ; GREPFIELD(static/info, '#k =      26', 8) ; 0.0
match ;     k-point 26 (z)     ; GREPFIELD(static/info, '#k =      26', 9) ; 0.0
Precision: 4.38e-05
match ;     Eigenvalue  1      ; GREPFIELD(static/info, '#k =      26', 3, 1) ; -0.875129
Precision: 2.25e-05
match ;     Eigenvalue  2      ; GREPFIELD(static/info, '#k =      26', 3, 2) ; -0.449557
Precision: 1.41e-04
match ;     Eigenvalue  3      ; GREPFIELD(static/info, '#k =      26', 3, 3) ; -0.28141
Precision: 1.41e-04
match ;     Eigenvalue  4      ; GREPFIELD(static/info, '#k =      26', 3, 4) ; -0.28141

# Dirac point
Precision: 1.67e-05
match ;     k-point 34 (x)     ; GREPFIELD(static/info, '#k =      34', 7) ; 0.333333
match ;     k-point 34 (y)     ; GREPFIELD(static/info, '#k =      34', 8) ; 0.333333
match ;     k-point 34 (z)     ; GREPFIELD(static/info, '#k =      34', 9) ; 0.0
Precision: 3.11e-04
match ;     Eigenvalue  1     ; GREPFIELD(static/info, '#k =      34', 3, 1) ; -0.62139
Precision: 3.10e-05
match ;     Eigenvalue  2     ; GREPFIELD(static/info, '#k =      34', 3, 2) ; -0.620636
Precision: 2.80e-05
match ;     Eigenvalue  3     ; GREPFIELD(static/info, '#k =      34', 3, 3) ; -0.560111
Precision: 8.38e-06
match ;     Eigenvalue  4     ; GREPFIELD(static/info, '#k =      34', 3, 4) ; -0.167682

Input      : 13-primitive.03-bcc_iron.inp
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1
  
Precision: 1.26e-12
match ;    Total energy          ; GREPFIELD(static/info, 'Total       =', 3) ; -125.87449919
Precision: 3.92e-07
match ;    Ion-ion energy        ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -78.31680646
Precision: 7.52e-13
match ;    Eigenvalues sum       ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -15.04703176
Precision: 1.64e-07
match ;    Hartree energy        ; GREPFIELD(static/info, 'Hartree     =', 3) ; 32.71841312
Precision: 8.21e-08
match ;    Exchange energy       ; GREPFIELD(static/info, 'Exchange    =', 3) ; -16.42276356
Precision: 6.86e-08
match ;    Correlation energy    ; GREPFIELD(static/info, 'Correlation =', 3) ; -1.3715089699999998
Precision: 3.00e-07
match ;    Kinetic energy        ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 60.05687022
Precision: 6.13e-08
match ;    External energy       ; GREPFIELD(static/info, 'External    =', 3) ; -122.53870375000001

Precision: 1.00e-04
match ;    k-point 2 (x)     ; GREPFIELD(static/info, '#k =       2', 7) ; 0.0
match ;    k-point 2 (y)     ; GREPFIELD(static/info, '#k =       2', 8) ; 0.5
match ;    k-point 2 (z)     ; GREPFIELD(static/info, '#k =       2', 9) ; 0.0
Precision: 1.43e-05
match ;    Eigenvalue  1     ; GREPFIELD(static/info, '#k =       2', 3, 1) ; -2.859431
Precision: 8.36e-06
match ;    Eigenvalue  2     ; GREPFIELD(static/info, '#k =       2', 3, 2) ; -1.6716069999999998
Precision: 8.30e-06
match ;    Eigenvalue  3     ; GREPFIELD(static/info, '#k =       2', 3, 3) ; -1.6596579999999999
Precision: 1.01e-16
match ;    Eigenvalue  4     ; GREPFIELD(static/info, '#k =       2', 3, 5) ; -0.010136000000000001
