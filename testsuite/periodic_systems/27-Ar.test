# -*- coding: utf-8 mode: shell-script -*-

Test       : Ar crystal - EM response
Program    : octopus
TestGroups : long-run, periodic_systems
Enabled    : Yes

Input : 27-Ar.01-gs.inp

match ; Total k-points   ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 8
match ; Reduced k-points ; GREPFIELD(static/info, 'Number of symmetry-reduced k-points', 6) ; 8
match ; Space group        ; GREPFIELD(out, 'Space group', 4) ; 221
match ; No. of symmetries  ; GREPFIELD(out, 'symmetries that can be used', 5)  ;  48
match ;  SCF convergence     ; GREPCOUNT(static/info, 'SCF converged') ; 1.0

Precision: 1.19e-07
match ;     Total energy           ; GREPFIELD(static/info, 'Total       =', 3) ; -23.84521606
Precision: 4.23e-07
match ;     Ion-ion energy         ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -8.46562672
Precision: 2.05e-07
match ;     Eigenvalues sum        ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -4.10539995
Precision: 6.26e-08
match ;     Hartree energy         ; GREPFIELD(static/info, 'Hartree     =', 3) ; 12.52936173
Precision: 1.80e-07
match ;     Exchange energy        ; GREPFIELD(static/info, 'Exchange    =', 3) ; -3.59236908
Precision: 2.26e-07
match ;     Correlation energy     ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.45253592
Precision: 9.10e-08
match ;     Kinetic energy         ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 18.19652461
Precision: 2.10e-07
match ;     External energy        ; GREPFIELD(static/info, 'External    =', 3) ; -42.06057068

Precision: 1.00e-04
match ;    k-point 1 (x)    ; GREPFIELD(static/info, '#k =       1', 7) ; 0.0
match ;    k-point 1 (y)    ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
match ;    k-point 1 (z)    ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 5.79e-06
match ;     Eigenvalue  1     ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -1.157489

Precision: 1.00e-04
match ;    k-point 2 (x)    ; GREPFIELD(static/info, '#k =       2', 7) ; 0.0
match ;    k-point 2 (y)    ; GREPFIELD(static/info, '#k =       2', 8) ; 0.5
match ;    k-point 2 (z)    ; GREPFIELD(static/info, '#k =       2', 9) ; 0.0
Precision: 5.79e-06
match ;     Eigenvalue  1     ; GREPFIELD(static/info, '#k =       2', 3, 1) ; -1.157488

Precision: 1.00e-04
match ;    k-point 3 (x)    ; GREPFIELD(static/info, '#k =       3', 7) ; 0.0
match ;    k-point 3 (y)    ; GREPFIELD(static/info, '#k =       3', 8) ; 0.0
match ;    k-point 3 (z)    ; GREPFIELD(static/info, '#k =       3', 9) ; 0.5
Precision: 5.79e-06
match ;     Eigenvalue  1     ; GREPFIELD(static/info, '#k =       3', 3, 1) ; -1.157488

Input : 27-Ar.02-kdotp.inp

Precision: 1.00e-04
match ;   vg(x) k1 st1    ; LINEFIELD(kdotp/velocity, 5, 3) ; 0.0
Precision: 1.00e-04
match ;   vg(x) k2 st1    ; LINEFIELD(kdotp/velocity, 14, 3) ; 0.0
Precision: 1.00e-04
match ;   vg(y) k3 st2   ; LINEFIELD(kdotp/velocity, 24, 4) ; 0.0
Precision: 3.00e-14
match ;   1/m*  k1 st1    ; GREPFIELD(kdotp/kpoint_1_1, 'Isotropic average', 3, 0) ; 0.599219
Precision: 1.00e-04
match ;   1/mxy k1 st1    ; GREPFIELD(kdotp/kpoint_1_1, 'State #1', 2, 1) ; 0.0

Input: 27-Ar.03-em_resp_mo.inp
if(available libxc_fxc); then
  Precision: 6.05e-06
  match ;         Susceptibility XX           ; GREPFIELD(em_resp/freq_0.0000/susceptibility, "susceptibility", 1, 1) ; -113.09833800000001
  Precision: 6.05e-06
  match ;         Susceptibility YY           ; GREPFIELD(em_resp/freq_0.0000/susceptibility, "susceptibility", 2, 2) ; -113.09833800000001
  Precision: 6.05e-06
  match ;         Susceptibility ZZ           ; GREPFIELD(em_resp/freq_0.0000/susceptibility, "susceptibility", 3, 3) ; -113.09833800000001
  Precision: 6.23e-06
  match ;         Re epsilon XX (LRC)         ; GREPFIELD(em_resp/freq_0.1000/epsilon, "Real", 1, 1) ; 1.246991
  Precision: 6.23e-06
  match ;         Re epsilon YY (LRC)         ; GREPFIELD(em_resp/freq_0.1000/epsilon, "Real", 2, 2) ; 1.246991
  Precision: 6.23e-06
  match ;         Re epsilon ZZ (LRC)         ; GREPFIELD(em_resp/freq_0.1000/epsilon, "Real", 3, 3) ; 1.246991
  Precision: 7.51e-06
  match ;         Im epsilon XX (LRC)         ; GREPFIELD(em_resp/freq_0.1000/epsilon, "Imaginary", 1, 1) ; 0.015017
  Precision: 7.51e-06
  match ;         Im epsilon YY (LRC)         ; GREPFIELD(em_resp/freq_0.1000/epsilon, "Imaginary", 2, 2) ; 0.015017
  Precision: 7.51e-06
  match ;         Im epsilon ZZ (LRC)         ; GREPFIELD(em_resp/freq_0.1000/epsilon, "Imaginary", 3, 3) ; 0.015017
  Precision: 7.51e-06
  match ;         Im epsilon av (LRC)         ; GREPFIELD(em_resp/freq_0.1000/epsilon, "Imaginary", 3, 4) ; 0.015017
  Precision: 6.23e-06
  match ;         Re eps XX (LRC, w/o G=0)        ; GREPFIELD(em_resp/freq_0.1000/epsilon, "LRC kernel", 1, 2) ; 1.246027
  Precision: 6.23e-06
  match ;         Re eps YY (LRC, w/o G=0)        ; GREPFIELD(em_resp/freq_0.1000/epsilon, "LRC kernel", 2, 3) ; 1.246027
  Precision: 6.23e-06
  match ;         Re eps ZZ (LRC, w/o G=0)        ; GREPFIELD(em_resp/freq_0.1000/epsilon, "LRC kernel", 3, 4) ; 1.246027
  Precision: 7.45e-06
  match ;         Im eps XX (LRC, w/o G=0)        ; GREPFIELD(em_resp/freq_0.1000/epsilon, "LRC kernel", 1, 8) ; 0.014899000000000003
  Precision: 7.45e-06
  match ;         Im eps YY (LRC, w/o G=0)        ; GREPFIELD(em_resp/freq_0.1000/epsilon, "LRC kernel", 2, 9) ; 0.014899000000000003
  Precision: 7.45e-06
  match ;         Im eps ZZ (LRC, w/o G=0)        ; GREPFIELD(em_resp/freq_0.1000/epsilon, "LRC kernel", 3, 10) ; 0.014899000000000003
  Precision: 7.45e-06
  match ;         Im eps av (LRC, w/o G=0)        ; GREPFIELD(em_resp/freq_0.1000/epsilon, "LRC kernel", 3, 11) ; 0.014899000000000003
  Precision: 2.50e-07
  match ;         Re epsilon at k-point           ; GREPFIELD(em_resp/freq_0.1000/epsilon_k_re, "  3  ", 14) ; 0.16823943
  Precision: 1.61e-06
  match ;         Re epsilon at k-point           ; GREPFIELD(em_resp/freq_0.1000/epsilon_k_re, "  3  ", 10) ; 0.3214342
  Precision: 1.69e-07
  match ;         Re epsilon at k-point           ; GREPFIELD(em_resp/freq_0.1000/epsilon_k_re, "  3  ", 11) ; 0.0
  Precision: 3.84e-07
  match ;         Im epsilon at k-point           ; GREPFIELD(em_resp/freq_0.1000/epsilon_k_im, "  2  ", 10) ; 0.0026133839
  Precision: 4.33e-07
  match ;         Im epsilon at k-point           ; GREPFIELD(em_resp/freq_0.1000/epsilon_k_im, "  4  ", 6) ; 0.00261341
  Precision: 1.10e-07
  match ;         Im epsilon at k-point           ; GREPFIELD(em_resp/freq_0.1000/epsilon_k_im, "  4  ", 7) ; 1.5571597e-08
  Precision: 5.50e-07
  match ;         Mag.-opt. Re alpha         ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'Re alpha', 4) ; -0.13485023
  Precision: 5.50e-07
  match ;         Mag.-opt. Re alpha         ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'Re alpha', 5) ; -0.13485023
  Precision: 5.50e-07
  match ;         Mag.-opt. Re alpha         ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'Re alpha', 6) ; -0.13485023
  Precision: 5.50e-07
  match ;         Mag.-opt. Re alpha         ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'Re alpha', 7) ; -0.13485023
  Precision: 2.70e-07
  match ;         MO Im alpha (w/o G=0)        ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'LRC kernel', 4, 3) ; 0.26553
  Precision: 2.70e-07
  match ;         MO Im alpha (w/o G=0)        ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'LRC kernel', 5, 3) ; 0.26553
  Precision: 2.70e-07
  match ;         MO Im alpha (w/o G=0)        ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'LRC kernel', 6, 3) ; 0.26553
  Precision: 2.70e-07
  match ;         MO Im alpha (w/o G=0)        ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'LRC kernel', 7, 3) ; 0.26553
  Precision: 2.81e-09
  match ;         Mag.-opt. Im epsilon         ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'Im epsilon', 7) ; 0.0027665666
  Precision: 2.81e-09
  match ;         Mag.-opt. Im epsilon         ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'Im epsilon', 8) ; 0.0027665666
  Precision: 2.81e-09
  match ;         Mag.-opt. Im epsilon         ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'Im epsilon', 9) ; 0.0027665666
  Precision: 2.81e-09
  match ;         Mag.-opt. Im epsilon         ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'Im epsilon', 10) ; 0.0027665666
  Precision: 5.66e-09
  match ;         MO Re eps (w/o G=0)          ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'LRC kernel', 7, 4) ; -0.0013935397499999999
  Precision: 5.66e-09
  match ;         MO Re eps (w/o G=0)          ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'LRC kernel', 8, 4) ; -0.0013935397499999999
  Precision: 5.66e-09
  match ;         MO Re eps (w/o G=0)          ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'LRC kernel', 9, 4) ; -0.0013935397499999999
  Precision: 5.66e-09
  match ;         MO Re eps (w/o G=0)          ; GREPFIELD(em_resp/freq_0.1000/alpha_mo, 'LRC kernel', 10, 4) ; -0.0013935397499999999
  Precision: 7.81e-09
  match ;         Mag.-opt. Re eps at kpt        ; GREPFIELD(em_resp/freq_0.1000/epsilon_mo_k, "  4   ", 8) ; -0.0012020495000000001
  Precision: 5.94e-09
  match ;         Mag.-opt. Im eps at kpt        ; GREPFIELD(em_resp/freq_0.1000/epsilon_mo_k, "  2   ", 11) ; 0.0026459218
  Precision: 4.70e-09
  match ;         Mag.-opt. Im eps at kpt        ; GREPFIELD(em_resp/freq_0.1000/epsilon_mo_k, "  2   ", 10) ; 0.0048026274

else
  match ; Error no libxc_fxc ; GREPCOUNT(err, 'not compiled with the fxc support') ; 1
endif
