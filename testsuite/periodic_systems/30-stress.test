# -*- coding: utf-8 mode: shell-script -*-

Test       : Stress tensor
Program    : octopus
TestGroups : short-run, periodic_systems
Enabled    : Yes


Processors : 4
Precision  : 1e-7

# TODO: UPDATE THE REFERENCE VALUES AFTER FIXING THE INDEPENDENT PARTICLE RUN https://gitlab.com/octopus-code/octopus/-/issues/696
# Input      : 30-stress.01-independent.inp

# match ;     Stress (11)      ; GREPFIELD(static/info, 'Total stress tensor', 2, 2) ; -0.005645947312
# match ;     Stress (12)      ; GREPFIELD(static/info, 'Total stress tensor', 3, 2) ; -0.006088082498
# match ;     Stress (13)      ; GREPFIELD(static/info, 'Total stress tensor', 4, 2) ; -0.00586914639
# match ;     Stress (21)      ; GREPFIELD(static/info, 'Total stress tensor', 2, 3) ; -0.00039022221429999996
# match ;     Stress (22)      ; GREPFIELD(static/info, 'Total stress tensor', 3, 3) ; -0.001457407419
# match ;     Stress (23)      ; GREPFIELD(static/info, 'Total stress tensor', 4, 3) ; -0.0005861471761
# match ;     Stress (31)      ; GREPFIELD(static/info, 'Total stress tensor', 2, 4) ; -0.0021573512889999998
# match ;     Stress (32)      ; GREPFIELD(static/info, 'Total stress tensor', 3, 4) ; 0.0007357126078
# match ;     Stress (33)      ; GREPFIELD(static/info, 'Total stress tensor', 4, 4) ; 0.0001390268731

# match ;   Pressure (H/b^3)   ; GREPFIELD(static/info, 'Pressure \[H/b^3\]', 4) ; 0.0058677254
# match ;   Pressure (GPa)     ; GREPFIELD(static/info, 'Pressure \[GPa\]', 8) ; 172.63450439

# match ;  Kinetic stress (11) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 2) ; 1.222914757E-02
# match ;  Kinetic stress (12) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 2) ; 7.178671366E-05
# match ;  Kinetic stress (13) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 2) ; -5.529174315E-05
# match ;  Kinetic stress (21) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 3) ; 7.178671366E-05
# match ;  Kinetic stress (22) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 3) ; 1.228532386E-02
# match ;  Kinetic stress (23) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 3) ; 6.562294732E-05
# match ;  Kinetic stress (31) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 4) ; -5.529174315E-05
# match ;  Kinetic stress (32) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 4) ; 6.562294732E-05
# match ;  Kinetic stress (33) ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 4) ; 1.226402391E-02

# match ;  Hartree stress (11) ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 2) ; 0.000686624679
# match ;  Hartree stress (12) ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 2) ; 0.000110529178
# match ;  Hartree stress (13) ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 2) ; -6.586250444e-05
# match ;  Hartree stress (21) ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 3) ; 0.000110529178
# match ;  Hartree stress (22) ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 3) ; 0.0008987790436999999
# match ;  Hartree stress (23) ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 3) ; 0.0001595756753
# match ;  Hartree stress (31) ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 4) ; -6.586250444e-05
# match ;  Hartree stress (32) ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 4) ; 0.0001595756753
# match ;  Hartree stress (33) ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 4) ; 0.000809552841

# match ;  XC stress (11)      ; GREPFIELD(static/stress, 'XC stress tensor', 2, 2) ; -9.675418234E-04
# match ;  XC stress (12)      ; GREPFIELD(static/stress, 'XC stress tensor', 3, 2) ; 0
# match ;  XC stress (13)      ; GREPFIELD(static/stress, 'XC stress tensor', 4, 2) ; 0
# match ;  XC stress (21)      ; GREPFIELD(static/stress, 'XC stress tensor', 2, 3) ; 0
# match ;  XC stress (22)      ; GREPFIELD(static/stress, 'XC stress tensor', 3, 3) ; -9.675418234E-04
# match ;  XC stress (23)      ; GREPFIELD(static/stress, 'XC stress tensor', 4, 3) ; 0
# match ;  XC stress (31)      ; GREPFIELD(static/stress, 'XC stress tensor', 2, 4) ; 0
# match ;  XC stress (32)      ; GREPFIELD(static/stress, 'XC stress tensor', 3, 4) ; 0
# match ;  XC stress (33)      ; GREPFIELD(static/stress, 'XC stress tensor', 4, 4) ; -9.675418234E-04

# match ; Pseudopotential stress (11) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 2, 2) ; 0.006049504587
# match ; Pseudopotential stress (12) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 3, 2) ; -0.0009211717667000001
# match ; Pseudopotential stress (13) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 4, 2) ; 0.00051617285
# match ; Pseudopotential stress (21) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 2, 3) ; 0.000146013438
# match ; Pseudopotential stress (22) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 3, 3) ; 0.005097509393
# match ; Pseudopotential stress (23) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 4, 3) ; -0.0013618088610000002
# match ; Pseudopotential stress (31) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 2, 4) ; -8.051288476e-05
# match ; Pseudopotential stress (32) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 3, 4) ; 0.0002093952519
# match ; Pseudopotential stress (33) ; GREPFIELD(static/stress, 'Pseudopotential stress tensor', 4, 4) ; 0.005499784882999999

# match ;  Ion-ion stress (11) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 2) ; -1.235178770E-02
# match ;  Ion-ion stress (12) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 2) ; 1.129036311E-03
# match ;  Ion-ion stress (13) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 2) ; -5.340350017E-04
# match ;  Ion-ion stress (21) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 3) ;  1.129036311E-03
# match ;  Ion-ion stress (22) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 3) ;  -1.122598798E-02
# match ;  Ion-ion stress (23) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 3) ;1.722698484E-03
# match ;  Ion-ion stress (31) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 4) ; -5.340350017E-04
# match ;  Ion-ion stress (32) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 4) ; 1.722698484E-03
# match ;  Ion-ion stress (33) ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 4) ;  -1.173667342E-02


Input      : 30-stress.02-gamma_point.inp

Precision: 2.83e-11
match ;        Pressure (H/b^3)        ; GREPFIELD(static/info, 'Pressure \[H/b^3\]', 4) ; 0.0005654916880000001
Precision: 1.8e-07
match ;        Pressure (GPa)          ; GREPFIELD(static/info, 'Pressure \[GPa\]', 8) ; 16.63734593
Precision: 4.68e-11
match ;       Kinetic stress (11)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 2) ; 0.009361514553
Precision: 1.00e-15
match ;       Kinetic stress (12)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 2) ; 0.0
Precision: 1.00e-15
match ;       Kinetic stress (13)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 2) ; 0.0
Precision: 1.00e-15
match ;       Kinetic stress (21)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 3) ; 0.0
Precision: 4.68e-11
match ;       Kinetic stress (22)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 3) ; 0.009361514554
Precision: 1.00e-15
match ;       Kinetic stress (23)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 3) ; 0.0
Precision: 1.00e-15
match ;       Kinetic stress (31)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 4) ; 0.0
Precision: 1.00e-15
match ;       Kinetic stress (32)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 4) ; 0.0
Precision: 4.68e-11
match ;       Kinetic stress (33)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 4) ; 0.009361514553
Precision: 4.43e-12
match ;       Hartree stress (11)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 2) ; 0.0008860627345999999
Precision: 1.00e-15
match ;       Hartree stress (12)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 2) ; 0.0
Precision: 1.00e-15
match ;       Hartree stress (13)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 2) ; 0.0
Precision: 1.00e-15
match ;       Hartree stress (21)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 3) ; 0.0
Precision: 4.43e-12
match ;       Hartree stress (22)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 3) ; 0.0008860627345999999
Precision: 1.00e-15
match ;       Hartree stress (23)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 3) ; 0.0
Precision: 1.00e-15
match ;       Hartree stress (31)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 4) ; 0.0
Precision: 1.00e-15
match ;       Hartree stress (32)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 4) ; 0.0
Precision: 4.43e-12
match ;       Hartree stress (33)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 4) ; 0.0008860627345999999
Precision: 1.65e-12
match ;       XC stress (11)           ; GREPFIELD(static/stress, 'XC stress tensor', 2, 2) ; -0.0037342883975000005
Precision: 1.00e-15
match ;       XC stress (12)           ; GREPFIELD(static/stress, 'XC stress tensor', 3, 2) ; 0.0
Precision: 1.00e-15
match ;       XC stress (13)           ; GREPFIELD(static/stress, 'XC stress tensor', 4, 2) ; 0.0
Precision: 1.00e-15
match ;       XC stress (21)           ; GREPFIELD(static/stress, 'XC stress tensor', 2, 3) ; 0.0
Precision: 1.65e-12
match ;       XC stress (22)           ; GREPFIELD(static/stress, 'XC stress tensor', 3, 3) ; -0.0037342883975000005
Precision: 1.00e-15
match ;       XC stress (23)           ; GREPFIELD(static/stress, 'XC stress tensor', 4, 3) ; 0.0
Precision: 1.00e-15
match ;       XC stress (31)           ; GREPFIELD(static/stress, 'XC stress tensor', 2, 4) ; 0.0
Precision: 1.00e-15
match ;       XC stress (32)           ; GREPFIELD(static/stress, 'XC stress tensor', 3, 4) ; 0.0
Precision: 1.65e-12
match ;       XC stress (33)           ; GREPFIELD(static/stress, 'XC stress tensor', 4, 4) ; -0.0037342883975000005
Precision: 5.75e-11
match ;       Ion-ion stress (11)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 2) ; -0.01149183583
Precision: 1.00e-15
match ;       Ion-ion stress (12)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 2) ; 0.0
Precision: 1.00e-15
match ;       Ion-ion stress (13)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 2) ; 0.0
Precision: 1.00e-15
match ;       Ion-ion stress (21)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 3) ; 0.0
Precision: 5.75e-11
match ;       Ion-ion stress (22)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 3) ; -0.01149183583
Precision: 1.00e-15
match ;       Ion-ion stress (23)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 3) ; 0.0
Precision: 1.00e-15
match ;       Ion-ion stress (31)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 4) ; 0.0
Precision: 1.00e-15
match ;       Ion-ion stress (32)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 4) ; 0.0
Precision: 5.75e-11
match ;       Ion-ion stress (33)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 4) ; -0.01149183583
Precision: 5.8e-12
match ;          Stress (11)           ; GREPFIELD(static/info, 'Total stress tensor', 2, 2) ; -0.0005654916880999999
Precision: 1.00e-15
match ;          Stress (12)           ; GREPFIELD(static/info, 'Total stress tensor', 3, 2) ; 0.0
Precision: 1.00e-15
match ;          Stress (13)           ; GREPFIELD(static/info, 'Total stress tensor', 4, 2) ; 0.0
Precision: 1.00e-15
match ;          Stress (21)           ; GREPFIELD(static/info, 'Total stress tensor', 2, 3) ; 0.0
Precision: 6e-12
match ;          Stress (22)           ; GREPFIELD(static/info, 'Total stress tensor', 3, 3) ; -0.0005654916881999999
Precision: 1.00e-15
match ;          Stress (23)           ; GREPFIELD(static/info, 'Total stress tensor', 4, 3) ; 0.0
Precision: 1.00e-15
match ;          Stress (31)           ; GREPFIELD(static/info, 'Total stress tensor', 2, 4) ; 0.0
Precision: 1.00e-15
match ;          Stress (32)           ; GREPFIELD(static/info, 'Total stress tensor', 3, 4) ; 0.0
Precision: 6e-12
match ;          Stress (33)           ; GREPFIELD(static/info, 'Total stress tensor', 4, 4) ; -0.0005654916880000001

Input      : 30-stress.03-par_kpoints.inp

Precision: 3.92e-11
match ;        Pressure (H/b^3)        ; GREPFIELD(static/info, 'Pressure \[H/b^3\]', 4) ; 0.000784376247
Precision: 1.15e-07
match ;        Pressure (GPa)          ; GREPFIELD(static/info, 'Pressure \[GPa\]', 8) ; 23.07715433
Precision: 5.91e-11
match ;       Kinetic stress (11)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 2) ; 0.011818605480000001
Precision: 5.33e-16
match ;       Kinetic stress (12)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 2) ; -1.0664019569999999e-07
Precision: 2.57e-13
match ;       Kinetic stress (13)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 2) ; -5.144847402e-05
Precision: 5.33e-16
match ;       Kinetic stress (21)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 3) ; -1.0664019569999999e-07
Precision: 6.63e-11
match ;       Kinetic stress (22)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 3) ; 0.01326567965
Precision: 6.10e-12
match ;       Kinetic stress (23)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 3) ; 0.000121967643
Precision: 2.57e-13
match ;       Kinetic stress (31)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 2, 4) ; -5.144847402e-05
Precision: 6.10e-12
match ;       Kinetic stress (32)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 3, 4) ; 0.000121967643
Precision: 6.58e-11
match ;       Kinetic stress (33)      ; GREPFIELD(static/stress, 'Kinetic stress tensor', 4, 4) ; 0.01316742171
Precision: 3.79e-12
match ;       Hartree stress (11)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 2) ; 0.0007577651179000001
Precision: 6.60e-13
match ;       Hartree stress (12)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 2) ; 0.00013201220310000002
Precision: 4.60e-13
match ;       Hartree stress (13)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 2) ; -9.208877863000001e-05
Precision: 6.60e-13
match ;       Hartree stress (21)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 3) ; 0.00013201220310000002
Precision: 4.86e-12
match ;       Hartree stress (22)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 3) ; 0.0009724328191
Precision: 9.00e-13
match ;       Hartree stress (23)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 3) ; 0.0001799918035
Precision: 4.60e-13
match ;       Hartree stress (31)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 2, 4) ; -9.208877863000001e-05
Precision: 9.00e-13
match ;       Hartree stress (32)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 3, 4) ; 0.0001799918035
Precision: 4.41e-11
match ;       Hartree stress (33)      ; GREPFIELD(static/stress, 'Hartree stress tensor', 4, 4) ; 0.0008819740040000001
Precision: 2.46e-11
match ;       XC stress (11)           ; GREPFIELD(static/stress, 'XC stress tensor', 2, 2) ; -0.004919494664
Precision: 7.10e-14
match ;       XC stress (12)           ; GREPFIELD(static/stress, 'XC stress tensor', 3, 2) ; -1.4196184710000002e-05
Precision: 4.16e-14
match ;       XC stress (13)           ; GREPFIELD(static/stress, 'XC stress tensor', 4, 2) ; 8.311047291000001e-06
Precision: 7.10e-14
match ;       XC stress (21)           ; GREPFIELD(static/stress, 'XC stress tensor', 2, 3) ; -1.4196184710000002e-05
Precision: 2.47e-11
match ;       XC stress (22)           ; GREPFIELD(static/stress, 'XC stress tensor', 3, 3) ; -0.004949842735
Precision: 1.23e-13
match ;       XC stress (23)           ; GREPFIELD(static/stress, 'XC stress tensor', 4, 3) ; -2.466540075e-05
Precision: 4.16e-14
match ;       XC stress (31)           ; GREPFIELD(static/stress, 'XC stress tensor', 2, 4) ; 8.311047291000001e-06
Precision: 1.23e-13
match ;       XC stress (32)           ; GREPFIELD(static/stress, 'XC stress tensor', 3, 4) ; -2.466540075e-05
Precision: 2.47e-11
match ;       XC stress (33)           ; GREPFIELD(static/stress, 'XC stress tensor', 4, 4) ; -0.004934135156
Precision: 8.25e-11
match ;       Ion-ion stress (11)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 2) ; -0.01649667662
Precision: 5.65e-12
match ;       Ion-ion stress (12)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 2) ; 0.001129036311
Precision: 2.67e-12
match ;       Ion-ion stress (13)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 2) ; -0.0005340350016999999
Precision: 5.65e-12
match ;       Ion-ion stress (21)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 3) ; 0.001129036311
Precision: 7.69e-16
match ;       Ion-ion stress (22)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 3) ; -0.01537087689
Precision: 8.61e-12
match ;       Ion-ion stress (23)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 3) ; 0.0017226984840000002
Precision: 2.67e-12
match ;       Ion-ion stress (31)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 2, 4) ; -0.0005340350016999999
Precision: 8.61e-12
match ;       Ion-ion stress (32)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 3, 4) ; 0.0017226984840000002
Precision: 7.94e-11
match ;       Ion-ion stress (33)      ; GREPFIELD(static/stress, 'Ion-ion stress tensor', 4, 4) ; -0.015881562329999998
Precision: 8.37e-13
match ;          Stress (11)           ; GREPFIELD(static/info, 'Total stress tensor', 2, 2) ; 0.0001674270314
Precision: 3.07e-18
match ;          Stress (12)           ; GREPFIELD(static/info, 'Total stress tensor', 3, 2) ; -0.00030731990450000003
Precision: 3.73e-13
match ;          Stress (13)           ; GREPFIELD(static/info, 'Total stress tensor', 4, 2) ; 7.451748862e-05
Precision: 3.07e-18
match ;          Stress (21)           ; GREPFIELD(static/info, 'Total stress tensor', 2, 3) ; -0.00030731990450000003
Precision: 6.76e-12
match ;          Stress (22)           ; GREPFIELD(static/info, 'Total stress tensor', 3, 3) ; -0.0013517331650000001
Precision: 2.76e-12
match ;          Stress (23)           ; GREPFIELD(static/info, 'Total stress tensor', 4, 3) ; -0.0005513513031
Precision: 3.73e-13
match ;          Stress (31)           ; GREPFIELD(static/info, 'Total stress tensor', 2, 4) ; 7.451748862e-05
Precision: 2.76e-12
match ;          Stress (32)           ; GREPFIELD(static/info, 'Total stress tensor', 3, 4) ; -0.0005513513031
Precision: 5.84e-12
match ;          Stress (33)           ; GREPFIELD(static/info, 'Total stress tensor', 4, 4) ; -0.001168822607

Input      : 30-stress.04-kpoint_sym.inp

Precision: 9.08e-11
match ;          Stress (11)           ; GREPFIELD(static/info, 'Total stress tensor', 2, 2) ; -0.0007937930124
Precision: 1.96e-10
match ;          Stress (22)           ; GREPFIELD(static/info, 'Total stress tensor', 3, 3) ; -0.0024021982
Precision: 2.08e-10
match ;          Stress (33)           ; GREPFIELD(static/info, 'Total stress tensor', 4, 4) ; -0.002180900739
Precision: 1.13e-10
match ;          Stress (12)           ; GREPFIELD(static/info, 'Total stress tensor', 3, 2) ; -0.0003125043092
Precision: 1.13e-10
match ;          Stress (21)           ; GREPFIELD(static/info, 'Total stress tensor', 2, 3) ; -0.0003125043092
Precision: 1.13e-10
match ;          Stress (23)           ; GREPFIELD(static/info, 'Total stress tensor', 4, 3) ; -0.0004141278134
Precision: 1.13e-10
match ;          Stress (32)           ; GREPFIELD(static/info, 'Total stress tensor', 3, 4) ; -0.0004141278134
Precision: 1.67e-10
match ;          Stress (31)           ; GREPFIELD(static/info, 'Total stress tensor', 2, 4) ; 5.9931886e-05
Precision: 1.67e-10
match ;          Stress (13)           ; GREPFIELD(static/info, 'Total stress tensor', 4, 2) ; 5.9931886e-05
Precision: 1.05e-10
match ;        Pressure (H/b^3)        ; GREPFIELD(static/info, 'Pressure \[H/b^3\]', 4) ; 0.00179229732
Precision: 3.07e-06
match ;        Pressure (GPa)          ; GREPFIELD(static/info, 'Pressure \[GPa\]', 8) ; 52.73122684

Input      : 30-stress.05-output_scf.inp

Precision: 6.27e-04
match ;         Pressure (H/b^3)         ; GREPFIELD(static/info, 'Pressure \[H/b^3\]', 4) ; 0.00059660218
Precision: 1.84e+01
match ;         Pressure (GPa)           ; GREPFIELD(static/info, 'Pressure \[GPa\]', 8) ; 17.5526485
Precision: 6.26e-04
match ;              Stress (xx)               ; GREPFIELD(static/info, 'T_{ij}', 2, 1) ; -0.0005964982447000001
Precision: 6.26e-04
match ;              Stress (yy)               ; GREPFIELD(static/info, 'T_{ij}', 3, 2) ; -0.0005966386180999999
Precision: 6.27e-04
match ;              Stress (zz)               ; GREPFIELD(static/info, 'T_{ij}', 4, 3) ; -0.0005966696717
Precision: 8e-07
match ;              Stress (xy)               ; GREPFIELD(static/info, 'T_{ij}', 3, 1) ; 0.0
match ;              Stress (yx)               ; GREPFIELD(static/info, 'T_{ij}', 2, 2) ; 0.0
match ;              Stress (yz)               ; GREPFIELD(static/info, 'T_{ij}', 4, 2) ; 0.0
match ;              Stress (zy)               ; GREPFIELD(static/info, 'T_{ij}', 3, 3) ; 0.0
match ;              Stress (zx)               ; GREPFIELD(static/info, 'T_{ij}', 2, 3) ; 0.0
match ;              Stress (xz)               ; GREPFIELD(static/info, 'T_{ij}', 4, 1) ; 0.0
