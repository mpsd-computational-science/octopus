foreach (test IN ITEMS
		01-quadratic_box
		02-fock-darwin
		03-helium_atom
		04-biot_savart
		05-spin_precession
		06-gdlib
		07-user_def_box
		08-constrain
)
	Octopus_add_test(${test}
			TEST_NAME ${curr_path}/${test}
	)
endforeach ()
