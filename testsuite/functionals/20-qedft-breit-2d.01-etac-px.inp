# 2D quantum ring 

FromScratch = yes
CalculationMode = gs

Dimensions = 2

# for 1 electron, not use any functional for e-e interaction
# one should turn this off when studying many electrons
XCFunctional = none

# for 1 electron, not include the Coulomb interaction
# one should comment this out when studying many electrons
XCPhotonIncludeHartree = no

BoxShape = parallelepiped
Spacing = 0.2

PseudopotentialSet = none

a = 20

%Lsize
 a | a
%

%Species 
  "ring" | species_user_defined | potential_formula | "0.39135*(x^2+y^2)+17.70*exp(-((x^2+y^2)/(0.997^2)))" | valence | 1
%

%Coordinates
  "ring" | 0 | 0
%

# mixing strategy, which depends on which system one study
MixField = density
Mixing = 0.1
MixingResidual = 0.0001
MixingRestart = 1

ConvRelDens = 1e-10
EigenSolverTolerance = 1e-26

MaximumIter = 300
Eigensolver = cg
CGEnergyChangeThreshold = 1e-28

%Output
  density | plane_z + axis_x + axis_y
  potential | plane_z + axis_x + axis_y
%

ExperimentalFeatures = yes

# to turn on the QEDFT px functional
XCPhotonFunctional = photon_xc_wfn

# PhotonXCEnergyMethod = 1: pxLDA (exchange virial relation)
# PhotonXCEnergyMethod = 2: px for one electron (do not use for many electrons)
PhotonXCEnergyMethod = expectation_value

# this is the eta_{c} factor introduced in our manuscript
PhotonXCEtaC = -5e-3


%PhotonModes
0.124992 | 0.005 | 1 | 0
%
