# -*- coding: utf-8 mode: shell-script -*-

Test       : ACBN0 functional for (AF) NiO and LiF bulk crystals
Program    : octopus
TestGroups : long-run, periodic_systems, lda_u
Enabled    : Yes


Input      : 02-ACBN0.01-nio.inp
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1
match ; Total k-points   ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 4

Precision: 5.0e-05
match ;      Total energy            ; GREPFIELD(static/info, 'Total       =', 3) ; -287.08673994000003
Precision: 8.85e-08
match ;      Ion-ion energy          ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -177.00987193
Precision: 6.0e-05
match ;      Eigenvalues sum         ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -37.25786551
Precision: 2.0e-05
match ;      Hartree energy          ; GREPFIELD(static/info, 'Hartree     =', 3) ; 87.88829173
Precision: 2.40e-06
match ;      Exchange energy         ; GREPFIELD(static/info, 'Exchange    =', 3) ; -34.01705334
Precision: 1.00e-07
match ;      Correlation energy      ; GREPFIELD(static/info, 'Correlation =', 3) ; -2.0087060500000002
Precision: 1.6e-05
match ;      Kinetic energy          ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 188.64515524
Precision: 3.5e-05
match ;      External energy         ; GREPFIELD(static/info, 'External    =', 3) ; -350.73205829
Precision: 6.0e-07
match ;       Hubbard energy           ; GREPFIELD(static/info, 'Hubbard     =', 3) ; 0.14750195

Precision: 1.00e-04
match ;   Total Magnetic Moment   ; GREPFIELD(static/info, 'mz = ', 3) ; 0.0
Precision: 9.38e-06
match ;   Local Magnetic Moment (Ni1)   ; GREPFIELD(static/info, '1        Ni', 3) ; 1.875656
match ;   Local Magnetic Moment (Ni2)   ; GREPFIELD(static/info, '2        Ni', 3) ; -1.875656
Precision: 1.10e-06
match ;   Local Magnetic Moment (O1)   ; GREPFIELD(static/info, '3         O', 3) ; 0.0
match ;   Local Magnetic Moment (O2)   ; GREPFIELD(static/info, '4         O', 3) ; 0.0
Precision: 7.50e-07
match ;   Occupation Ni2 down 3d4   ; LINEFIELD(static/occ_matrices, -2, 7) ; 0.97326698
Precision: 4.98e-07
match ;   Occupation Ni2 down 3d5   ; LINEFIELD(static/occ_matrices, -1, 9) ; 0.9951783

Precision: 2.73e-05
match ;   Ueff 3d Ni1   ; LINEFIELD(static/effectiveU, -10, 4) ; 0.546672
match ;   Ueff 3d Ni2   ; LINEFIELD(static/effectiveU, -9, 4) ; 0.546672
Precision: 4.05e-05
match ;   U 3d Ni1      ; LINEFIELD(static/effectiveU, -6, 4) ; 0.809762
match ;   U 3d Ni2      ; LINEFIELD(static/effectiveU, -5, 4) ; 0.809762
Precision: 1.32e-04
match ;   J 3d Ni1      ; LINEFIELD(static/effectiveU, -2, 4) ; 0.26309
match ;   J 3d Ni2      ; LINEFIELD(static/effectiveU, -1, 4) ; 0.26309

Precision: 4.28e-05
match ;   Kanamori U Ni1    ; LINEFIELD(static/kanamoriU, -10, 4) ; 0.855651
match ;   Kanamori U Ni2    ; LINEFIELD(static/kanamoriU, -9, 4) ; 0.855651
Precision: 3.41e-05
match ;   Kanamori Up Ni1   ; LINEFIELD(static/kanamoriU, -6, 4) ; 0.681795
match ;   Kanamori Up Ni2   ; LINEFIELD(static/kanamoriU, -5, 4) ; 0.681795
Precision: 1.33e-05
match ;   Kanamori J Ni1    ; LINEFIELD(static/kanamoriU, -2, 4) ; 0.026551000000000005
match ;   Kanamori J Ni2    ; LINEFIELD(static/kanamoriU, -1, 4) ; 0.026551000000000005

Precision: 1.00e-04
match ;    k-point 1 (x)    ; GREPFIELD(static/info, '#k =       1', 7) ; 0.0
match ;    k-point 1 (y)    ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
match ;    k-point 1 (z)    ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 1.02e-05
match ;    Eigenvalue  1    ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -2.0494950000000003
Precision: 9.72e-06
match ;    Eigenvalue  8    ; GREPFIELD(static/info, '#k =       1', 3, 8) ; -1.9433259999999999
Precision: 4.24e-05
match ;    Eigenvalue 16    ; GREPFIELD(static/info, '#k =       1', 3, 16) ; -0.847906
Precision: 4.23e-05
match ;    Eigenvalue 17    ; GREPFIELD(static/info, '#k =       1', 3, 17) ; -0.845538

Precision: 2.01e-04
match ;        Force 1 (x)                 ; GREPFIELD(static/info, 'Forces on the ions', 3, 2) ; -4.831706e-05
Precision: 2.53e-04
match ;        Force 1 (y)                 ; GREPFIELD(static/info, 'Forces on the ions', 4, 2) ; -7.4700006e-05
Precision: 2.60e-04
match ;        Force 1 (z)                 ; GREPFIELD(static/info, 'Forces on the ions', 5, 2) ; 0.00016526055200000002
Precision: 5.76e-05
match ;        Force 2 (x)                 ; GREPFIELD(static/info, 'Forces on the ions', 3, 3) ; -0.00020346419600000002
Precision: 1.08e-04
match ;        Force 2 (y)                 ; GREPFIELD(static/info, 'Forces on the ions', 4, 3) ; 0.000146910031
Precision: 6.22e-05
match ;        Force 2 (z)                 ; GREPFIELD(static/info, 'Forces on the ions', 5, 3) ; 4.3293736e-05
Precision: 7.31e-05
match ;        Force 3 (x)                 ; GREPFIELD(static/info, 'Forces on the ions', 3, 4) ; 0.000127552424
Precision: 8.65e-05
match ;        Force 3 (y)                 ; GREPFIELD(static/info, 'Forces on the ions', 4, 4) ; -3.6095782e-05
Precision: 1.02e-04
match ;        Force 3 (z)                 ; GREPFIELD(static/info, 'Forces on the ions', 5, 4) ; -0.00010627424300000001
Precision: 7.29e-05
match ;        Force 4 (x)                 ; GREPFIELD(static/info, 'Forces on the ions', 3, 5) ; 0.00012748147100000002
Precision: 8.65e-05
match ;        Force 4 (y)                 ; GREPFIELD(static/info, 'Forces on the ions', 4, 5) ; -3.6114243800000004e-05
Precision: 1.02e-04
match ;        Force 4 (z)                 ; GREPFIELD(static/info, 'Forces on the ions', 5, 5) ; -0.000106178111


Processors : 4

Input      : 02-ACBN0.02-lif.inp
match ;  SCF convergence  ; GREPCOUNT(static/info, 'SCF converged') ; 1.0

Precision: 1.22e-07
match ;    Total energy          ; GREPFIELD(static/info, 'Total       =', 3) ; -24.30860587
Precision: 7.21e-08
match ;    Ion-ion energy        ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -14.42250723
Precision: 2.25e-07
match ;    Eigenvalues sum       ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -4.50303451
Precision: 4.14e-07
match ;    Hartree energy        ; GREPFIELD(static/info, 'Hartree     =', 3) ; 8.278795129999999
Precision: 2.19e-07
match ;    Exchange energy       ; GREPFIELD(static/info, 'Exchange    =', 3) ; -4.3822858700000005
Precision: 2.47e-05
match ;    Correlation energy    ; GREPFIELD(static/info, 'Correlation =', 3) ; -0.493492
Precision: 8.82e-08
match ;    Kinetic energy        ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 17.63100246
Precision: 2.75e-07
match ;    Hubbard energy        ; GREPFIELD(static/info, 'Hubbard     =', 3) ; 0.054931280000000006

Precision: 1.00e-04
match ;    Total Magnetic Moment    ; GREPFIELD(static/info, 'mz = ', 3) ; 0.0
match ;    Local Magnetic Moment (Li)    ; GREPFIELD(static/info, '1        Li', 3) ; 0.0
match ;    Local Magnetic Moment (F)     ; GREPFIELD(static/info, '2         F', 3) ; 0.0

Precision: 4.81e-07
match ;    Occupation F down 2p2    ; LINEFIELD(static/occ_matrices, -2, 3) ; 0.96156789
match ;    Occupation F down 2p3    ; LINEFIELD(static/occ_matrices, -1, 5) ; 0.96156789

Precision: 2.48e-05
match ;    U2p F      ; LINEFIELD(static/effectiveU, -7, 4) ; 0.495478
Precision: 4.32e-05
match ;    Kanamori U      ; LINEFIELD(static/kanamoriU, -7, 4) ; 0.863862
Precision: 3.90e-05
match ;    Kanamori Up     ; LINEFIELD(static/kanamoriU, -4, 4) ; 0.780084
Precision: 4.22e-16
match ;    Kanamori J      ; LINEFIELD(static/kanamoriU, -1, 4) ; 0.042203000000000004

Precision: 1.00e-04
match ;     k-point 2 (x)     ; GREPFIELD(static/info, '#k =       2', 7) ; 0.0
match ;     k-point 2 (y)     ; GREPFIELD(static/info, '#k =       2', 8) ; 0.5
match ;     k-point 2 (z)     ; GREPFIELD(static/info, '#k =       2', 9) ; 0.0
Precision: 4.71e-05
match ;     Eigenvalue  1 up     ; GREPFIELD(static/info, '#k =       2', 3, 1) ; -0.942058
Precision: 4.71e-05
match ;     Eigenvalue  1 dn     ; GREPFIELD(static/info, '#k =       2', 3, 2) ; -0.942058
Precision: 2.05e-05
match ;     Eigenvalue  3 up     ; GREPFIELD(static/info, '#k =       2', 3, 5) ; -0.410708
Precision: 2.05e-05
match ;     Eigenvalue  4 up     ; GREPFIELD(static/info, '#k =       2', 3, 7) ; -0.410708
Precision: 1.42e-05
match ;     Eigenvalue  5 up     ; GREPFIELD(static/info, '#k =       2', 3, 9) ; 0.283088

Precision: 1.5e-12
match ;      Force 1 (x)              ; GREPFIELD(static/info, 'Forces on the ions', 3, 2) ; 0.0
match ;      Force 1 (y)              ; GREPFIELD(static/info, 'Forces on the ions', 4, 2) ; 0.0
match ;      Force 1 (z)              ; GREPFIELD(static/info, 'Forces on the ions', 5, 2) ; 0.0
match ;      Force 2 (x)              ; GREPFIELD(static/info, 'Forces on the ions', 3, 3) ; 0.0
match ;      Force 2 (y)              ; GREPFIELD(static/info, 'Forces on the ions', 4, 3) ; 0.0
match ;      Force 2 (z)              ; GREPFIELD(static/info, 'Forces on the ions', 5, 3) ; 0.0
