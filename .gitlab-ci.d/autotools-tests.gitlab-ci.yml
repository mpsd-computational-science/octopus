include:
  - '.gitlab-ci.d/module-loading.gitlab-ci.yml'
  - '.gitlab-ci.d/autotools-variables.gitlab-ci.yml'
  - '.gitlab-ci.d/autotools-compilation.gitlab-ci.yml'

##### foss #####

.foss_autotools_template:
  parallel:
    matrix:
      - TOOLCHAIN_VERSION:
          - foss2022a-serial
          - foss2023a-serial
          - foss2023b-serial
  extends:
    - .autotools_compile_test
    - .foss_configure_flags_serial
  variables:
    OCTOPUS_DEPENDENCIES: octopus-dependencies/full
    MPSD_SOFTWARE_RELEASE: 24a

.foss_mpi_autotools_template:
  parallel:
    matrix:
      - TOOLCHAIN_VERSION:
          - foss2022a-mpi
          - foss2023a-mpi
          # - foss2023b-mpi  # tolerance issues
  extends:
    - .autotools_compile_test
    - .foss_configure_flags_serial
    - .foss_configure_flags_mpi
  variables:
    OCTOPUS_DEPENDENCIES: octopus-dependencies/full
    OCT_TEST_NJOBS: 8
    MPSD_SOFTWARE_RELEASE: 24a

### serial ###

foss_min_autotools:
  extends:
    - .foss_autotools_template
    - .foss_configure_flags_serial_min
  variables:
    OCTOPUS_DEPENDENCIES: libxc gsl
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.foss-compiler-flags-standard]

foss_autotools:
  extends: .foss_autotools_template
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.foss-compiler-flags-standard]


foss_omp_autotools:
  extends: .foss_autotools_template
  variables:
    OMP_NUM_THREADS: 2
    OCT_TEST_NJOBS: 8
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.foss-compiler-flags-opt]
    - export CONFIGURE_FLAGS="$CONFIGURE_FLAGS --enable-openmp"
  rules:
    - if: $TOOLCHAIN_VERSION == "foss2023a-serial"

foss_debug_autotools:
  extends: .foss_autotools_template
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - module unload dftbplus  # floating point exception in dftbplus in debug mode
    - !reference [.foss-compiler-flags-debug]
    # remove dftbplus from configure flags
    - CONFIGURE_FLAGS=`echo $CONFIGURE_FLAGS | sed -Ee 's/--with-dftbplus-prefix[^ ]+ //'`
  rules:
    - if: $TOOLCHAIN_VERSION == "foss2023a-serial"

foss_opt_autotools:
  extends: .foss_autotools_template
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.foss-compiler-flags-opt]
    - export CONFIGURE_FLAGS="$CONFIGURE_FLAGS --disable-debug"
  rules:
    - if: $TOOLCHAIN_VERSION == "foss2023a-serial"

foss_cuda_autotools:
  extends:
    - .autotools_compile_test
    - .foss_configure_flags_serial
  variables:
    TOOLCHAIN_VERSION: foss2022a-cuda-mpi
    OCTOPUS_DEPENDENCIES: octopus-dependencies/full
    SCHEDULER_PARAMETERS: "--ntasks=1 --cpus-per-task=16 --partition=gpu --gpus-per-task=4"
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.foss-compiler-flags-standard]
    - export CONFIGURE_FLAGS="$CONFIGURE_FLAGS --enable-cuda --with-cuda-prefix=$MPSD_CUDA_ROOT"
    - FCFLAGS="$FCFLAGS -fallow-argument-mismatch"


foss_valgrind_autotools:
  extends:
    - .autotools_compile_test
    - .foss_configure_flags_valgrind
  parallel:
    matrix:
      - TOOLCHAIN_VERSION:
          - foss2022a-serial
          - foss2023a-serial
  variables:
    OCTOPUS_DEPENDENCIES: octopus-dependencies/full
    EXEC: valgrind --track-origins=yes --leak-check=full --show-leak-kinds=all --errors-for-leak-kinds=all --gen-suppressions=all --error-exitcode=1 --suppressions=$CI_PROJECT_DIR/.gitlab-ci.d/valgrind_suppressions/spack_${TOOLCHAIN_VERSION}_valgrind.supp
    LOCAL_MAKEFLAGS: -j 16
    OCT_TEST_NJOBS: 16
    SCHEDULER_PARAMETERS: "--ntasks=1 --cpus-per-task=16 --partition=public --time=4:00:00"
    TEST_TYPE: check-short
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.foss-compiler-flags-valgrind]

# TODO; ppc currently not in slurm
foss_ppc_autotools:
  extends:
    - .autotools_compile_test
    - .foss_configure_flags_serial
    # may need modifications:
    # --with-blas="-L$MPSD_OPENBLAS_ROOT -lopenblas -pthread" [additional -pthread]
    # --without-cgal
  variables:
    TOOLCHAIN_VERSION: foss2022a-serial
    OCTOPUS_DEPENDENCIES: octopus-dependencies/full
    SCHEDULER_PARAMETERS: "--ntasks=1 --cpus-per-task=16 --partition=TODO"
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.foss-compiler-flags-ppc]
  when: manual

### mpi ###

# mpi-compiler-env-variables has te be referenced AFTER load-modules:
# loading modules overwrites CC, FC etc.

foss_mpi_min_autotools:
  extends:
    - .foss_mpi_autotools_template
    - .foss_configure_flags_serial_min
    - .foss_configure_flags_mpi_min
  variables:
    OCTOPUS_DEPENDENCIES: libxc gsl elpa
    OCT_TEST_NJOBS: 8
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.foss-compiler-flags-standard]
    - !reference [.foss-compiler-mpi]
    - export CONFIGURE_FLAGS="$CONFIGURE_FLAGS $MPI_CONFIGURE_FLAGS"

foss_mpi_autotools:
  extends: .foss_mpi_autotools_template
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.foss-compiler-flags-standard]
    - !reference [.foss-compiler-mpi]
    - export CONFIGURE_FLAGS="$CONFIGURE_FLAGS $MPI_CONFIGURE_FLAGS"

foss_mpi_debug_autotools:
  extends: .foss_mpi_autotools_template
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.foss-compiler-flags-debug]
    - !reference [.foss-compiler-mpi]
    - export CPPFLAGS="-DINIT_BATCH"
    - export FCFLAGS="-g -fno-var-tracking-assignments -Wall -Wno-maybe-uninitialized -Wno-surprising -O2 -march=native -fbacktrace -fcheck=all -fbounds-check -finit-real=snan -ffpe-trap=zero,invalid -fallow-argument-mismatch -ftest-coverage -fprofile-arcs -fprofile-update=single -ffree-line-length-none"
    - export CONFIGURE_FLAGS="$CONFIGURE_FLAGS $MPI_CONFIGURE_FLAGS"
  rules:
    - if: $TOOLCHAIN_VERSION =~ "foss2023a-mpi"

foss_mpi_omp_autotools:
  extends: .foss_mpi_autotools_template
  variables:
    OCT_TEST_NJOBS: 4
    OMP_NUM_THREADS: 2
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.foss-compiler-flags-opt]
    - !reference [.foss-compiler-mpi]
    - export CONFIGURE_FLAGS="$CONFIGURE_FLAGS $MPI_CONFIGURE_FLAGS --enable-openmp"
  rules:
    - if: $TOOLCHAIN_VERSION =~ "foss2023a-mpi"

foss_mpi_opt_autotools:
  extends: .foss_mpi_autotools_template
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.foss-compiler-flags-opt]
    - !reference [.foss-compiler-mpi]
    - export CONFIGURE_FLAGS="$CONFIGURE_FLAGS $MPI_CONFIGURE_FLAGS --disable-debug"
  rules:
    - if: $TOOLCHAIN_VERSION =~ "foss2023a-mpi"

foss_cuda_mpi_autotools:
  extends:
    - .autotools_compile_test
    - .foss_configure_flags_serial
  variables:
    TOOLCHAIN_VERSION: foss2022a-cuda-mpi
    OCTOPUS_DEPENDENCIES: octopus-dependencies/full
    OCT_TEST_NJOBS: 4
    OMP_NUM_THREADS: 2
    SCHEDULER_PARAMETERS: "--ntasks=1 --cpus-per-task=16 --partition=gpu --gpus-per-task=4"
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.foss-compiler-flags-opt]
    - !reference [.foss-compiler-mpi]
    - export CONFIGURE_FLAGS="$CONFIGURE_FLAGS $MPI_CONFIGURE_FLAGS --enable-openmp --enable-cuda --with-cuda-prefix=$MPSD_CUDA_ROOT"
    - FCFLAGS="$FCFLAGS -fallow-argument-mismatch"


##### INTEL  #####

.intel_autotools_template:
  extends:
    - .autotools_compile_test
    - .intel_configure_flags_serial
  variables:
    TOOLCHAIN_VERSION: intel2023a-serial
    OCTOPUS_DEPENDENCIES: octopus-dependencies/full

.intel_mpi_autotools_template:
  extends:
    - .autotools_compile_test
    - .intel_configure_flags_serial
    - .intel_configure_flags_mpi
  variables:
    TOOLCHAIN_VERSION: intel2023a-mpi
    OCTOPUS_DEPENDENCIES: octopus-dependencies/full
    OCT_TEST_NJOBS: 8

### serial ###

intel_min_autotools:
  extends:
    - .intel_autotools_template
    - .intel_configure_flags_serial_min
  variables:
    OCTOPUS_DEPENDENCIES: libxc gsl
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.intel-compiler-serial]
    - !reference [.intel-compiler-flags-non_openmp]
  when: manual

intel_autotools:
  extends: .intel_autotools_template
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.intel-compiler-serial]
    - !reference [.intel-compiler-flags-non_openmp]

# TODO ADD INTEL OPENMP JOB

### mpi ###

intel_mpi_min_autotools:
  extends:
    - .intel_mpi_autotools_template
    - .intel_configure_flags_serial_min
    - .intel_configure_flags_mpi_min
  variables:
    OCTOPUS_DEPENDENCIES: libxc gsl
    OCT_TEST_NJOBS: 8
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.intel-compiler-mpi]
    - !reference [.intel-compiler-flags-non_openmp]
    - export CONFIGURE_FLAGS="$CONFIGURE_FLAGS $MPI_CONFIGURE_FLAGS"
  when: manual

intel_mpi_autotools:
  extends: .intel_mpi_autotools_template
  before_script:
    - mpsd-show-job-resources
    - !reference [.load-modules]
    - !reference [.intel-compiler-mpi]
    - !reference [.intel-compiler-flags-non_openmp]
    - export CONFIGURE_FLAGS="$CONFIGURE_FLAGS $MPI_CONFIGURE_FLAGS"

# TODO ADD INTEL OPENMP JOB
