#!/bin/bash
module purge
module load gcc/4.4
module load openmpi/1.6.3

export LIBSDIR='/sw/openmpi/'

export SOFTWARE_DIR="$HOME/SOFTWARE/exec"

export FC=mpif90
export CC=mpicc
export CXX=mpicxx

export STDFLAGS="-O3 -mcpu=power7 -mtune=power7"

export FCFLAGS=$STDFLAGS
export CFLAGS=$STDFLAGS
export CXXLAGS=$STDFLAGS

export C_TYPE="gnu64-4.4-essl"

export branch=`awk '{print $2}' ../.git/HEAD`
export branch=`basename $branch`

INSTALLDIR="$SOFTWARE_DIR/octopus/git-$C_TYPE/$branch"

#GSL  1.16 --compiled with gcc 4.4.6
export GSL_HOME="$LIBSDIR/GSL/1.16/"

#LAPACK gcc 4.4
export LAPACK_HOME="$LIBSDIR/LAPACK/3.4.2-gnu64-4.4"

#PARMETIS gcc 4.7.2
export PARMETIS_HOME="$LIBSDIR/METIS/PARMETIS-4.0.3/"

#METIS gcc 4.7.2
export METIS_HOME="$LIBSDIR/METIS/METIS-5.1.0/"

#FFTW3.3.4 gcc 4.4
export FFTW_HOME="$LIBSDIR/FFTW/3.3.4-gnu64-4.4"
export LD_LIBRARY_PATH=$FFTW_HOME/lib:$LD_LIBRARY_PATH

#SCALAPACK gcc 4.4.6
export SCALAPACK_HOME="$LIBSDIR/SCALAPACK/2.0.2-gnu64-4.4/"
export LD_LIBRARY_PATH=$SCALAPACK_HOME/lib:$LD_LIBRARY_PATH

#LIBXC
export LIBXC_HOME="$SOFTWARE_DIR/libxc/3.0.0-$C_TYPE/"

#LIBVDWXC
export LIBVDWXC_HOME="$SOFTWARE_DIR/libvdwxc/0.2.0-$C_TYPE/"
export CPPFLAGS="-I$LIBVDWXC_HOME/include -DHAVE_LIBVDWXC"
#export LIBS=-lvdwxcfort
export LDFLAGS="-L$LIBVDWXC_HOME/lib -lvdwxcfort -Wl,-rpath=$LIBVDWXC_HOME/lib"
export LD_LIBRARY_PATH=$LIBVDWXC_HOME/lib:$LD_LIBRARY_PATH

export LD_LIBRARY_PATH=/opt/ibmcmp/lib64/:/opt/ibmcmp/xlsmp/2.1/lib64/:/opt/ibmcmp/xlf/13.1/lib64/:$LD_LIBRARY_PATH

export LIBS_BLAS=" -L$SCALAPACK_HOME/lib -L$LAPACK_HOME/lib -lscalapack -llapack -lessl -lblacssmp -L$FFTW_HOME/lib -lfftw3 -lfftw3_threads -lfftw3_mpi / opt/gnu/gcc/4.4.6/lib64/libgfortran.a -L/opt/ibmcmp/xlf/13.1/lib64/ -lxlf90_r -lxlfmath -lxl -L/opt/ibmcmp/xlsmp/2.1/lib64/ -lxlomp_ser -Wl,--rpath /opt/ibmcmp/xlf/13.1/lib64/ /opt/ibmcmp/xlf/13.1/lib64/libxl.a -R/opt/ibmcmp/lib64/"

make distclean
make clean
../configure --prefix=$INSTALLDIR \
    --disable-gdlib --disable-gsltest \
    --with-libxc-prefix=$LIBXC_HOME \
    --with-fftw-prefix=$FFTW_HOME \
    --with-gsl-prefix=$GSL_HOME \
    --with-metis-prefix=$METIS_HOME\
    --with-parmetis-prefix=$PARMETIS_HOME \
    --enable-mpi \
    --enable-openmp
