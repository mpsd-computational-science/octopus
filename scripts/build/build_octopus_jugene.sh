 #!/bin/bash
module load gsl;
module load lapack;
autoreconf -i;
export FC_INTEGER_SIZE=4;
export CC_FORTRAN_INT=int;
export CC=mpixlc_r;
export CFLAGS='-g -O3 -qarch=450d';
export FC=mpixlf90_r;
export FCFLAGS='-g -O3 -qarch=450d -qxlf90=autodealloc -qessl -qsmp=omp';
export LIBS_BLAS="-lesslsmpbg -L/opt/ibmmath/essl/4.4/lib -lesslbg";
export LIBS_LAPACK="-L$LAPACK_LIB -llapack";
export LIBS_FFT="-L/bgsys/local/fftw3/lib/ -lfftw3 -lm";
./configure --prefix=$outdir \
    --host=powerpc32-unknown-linux-gnu \
    --build=powerpc64-unknown-linux-gnu \
    --disable-gdlib \
    --with-gsl-prefix=$GSL_DIR \
    --enable-mpi --enable-openmp;
