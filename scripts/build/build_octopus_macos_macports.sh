#!/bin/bash

## NOTE: THIS SCRIPT IS NOT TESTED AND IS NOT USED IN THE CI. THE USER WILL BE ASKED WHETHER TO PROCEED

# Set language (which is a bit tricky for MacOS)
export LANG="en_US"
export LC_CTYPE="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"

# Determine the number of available processors. It is better to use only physical cpus
n_proc=$(sysctl -n hw.physicalcpu)
# Determine MacPorts prefix
mac_ports="/opt/local"
# Define the current working directory
PWD=$(pwd)
# Define $PATH variable. this will override what is present so that this installtion uses either MacPorts toolchains (preferred) or Apple Toolchain
# the final path will be: PATH=/opt/local/bin:/opt/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin:<the_rest_of_the_original_path> 
export PATH="$mac_ports/bin:$mac_ports/sbin:/usr/bin:/usr/sbin:/bin/sbin:$PATH"

print_usage()
{
	printf 'Welcome to Octopus compile script for MacOS!\n'
  printf '\t%s\n' "--prefix: installation path (default: './installed/')"
	printf '\t%s\n' "--root-path: path to the root of octopus repository (default: 'parent_of_pwd' (not ".."))"
	printf '\t%s\n' "-l, --install-libs: install required libraries (gcc, openmpi, autotools, libxc, gsl, fftw, lapack, scalapack). MacPorts is required. Default: no"
	printf '\t%s\n' "-s, --skip-config: skip the autoreconf and the configure command. Default: no"
  printf '\t%s\n' "-d, --debug: Compile with debug flags. Default: no"
	printf '\t%s\n' "-h, --help: print help (this list!)"
}

# Parse command shell line to detect arguments
_arg_prefix="$PWD/installed" # Installation path
_arg_root_path="${PWD%/*}"   # Root path, where the Octopus repository is
_arg_install_libs='no'
_arg_skip_config='no'
_arg_debug='no'

while [[ $# -gt 0 ]]; do
  _key="$1"
  case "$_key" in
    # Define installation path
    --prefix)
      if [[ $# -lt 2 || $2 == -* ]]; then
        die "Missing value for the optional argument '$_key'." 1
      fi
      _arg_prefix="$2"
      shift # past argument
      ;;
    --prefix=*)
      _arg_prefix="${_key##--prefix=}"
      ;;
    # Define root path for Octopus
    --root-path)
      if [[ $# -lt 2 || $2 == -* ]]; then
        die "Missing value for the optional argument '$_key'." 1
      fi
      _arg_root_path="$2"
      shift # past argument
      ;;
    --root-path=*)
      _arg_root_path="${_key##--root-path=}"
      ;;
    -l|--install-libs)
      _arg_install_libs="yes"
      ;;
    -s|--skip-config)
      _arg_skip_config="yes"
      ;;
    -d|--debug)
      _arg_debug="yes"
      ;;
    -h|--help)
      print_usage
      return 0
      ;;
    *)
      print_usage
      die "Unkown argument" 1
      ;;
  esac
  shift # past argument or value
done

# THIS SCRIPT IS NOT TESTED AND IS NOT USED IN THE CI. ASK THE USER WHETHER TO PROCEED
printf "Welcome to Octopus compile script for MacOS!\n"
printf "This script is experimental and not automatically tested in the CIs, thus the installation might fail.\n"
printf "Even if the installation is successful, it is still likely that many tests in 'make check' will fail due to numerical fluctuations and lack of certain libraries (e.g. CGAL).\n"
printf "While this is acceptable for development, we recommend cautiousness when running Octopus for production calculations.\n"
printf "Do you want to proceed with the installation? (y/n): "

possible_confirmation_values=("y" "Y" "n" "N")
confirmation_is_valid='no'

while [[ $confirmation_is_valid == 'no' ]]; do
  read confirm_proceed
  # First, check that the user input is valid (y, Y, n, N)
  if [[ " ${possible_confirmation_values[@]} " =~ " $confirm_proceed " ]]; then
    confirmation_is_valid='yes'
    # Convert to lower case
    test_lower=$(tr '[:upper:]' '[:lower:]' <<< "$confirm_proceed")
    if [[ "$confirm_proceed" == "y" ]]; then
      echo "Proceeding..."
    elif [[ "$confirm_proceed" == "n" ]]; then
      echo "Aborting..."
      exit 0
    fi
  else
    printf "Invalid answer. Please answer 'y' to proceed or 'n' to abort: "
  fi
done

# Test that root path was set correctly
if [ ! -f "$_arg_root_path/configure.ac" ]; then
	echo "Error: Could not find configure.ac in octopus repository path ($_arg_root_path). Please pass the right path to the compilation script using the \"--root-path\" flag"
	exit 1
fi

if [[ "$_arg_install_libs" == "yes" ]]; then
  # -N stands for non-interactive, meaning that dependencies will be automatically installed
  sudo port -N install gcc12 autoconf automake libtool
  sudo port -N install openmpi-gcc12 +fortran
  sudo port -N install libxc5 +fortran
  sudo port -N install gsl +gcc12
  sudo port -N install fftw-3 +openmpi
  sudo port -N install cgal5
  sudo port -N install netcdf-fortran +gcc12
  sudo port -N install lapack +gcc12
  sudo port -N install scalapack +openmpi
fi

# Compiler flags (Optimization and debugging info)
if [[ "$_arg_debug" == "yes" ]]; then
  export CFLAGS="-O2 -g -Wall"
  export FCFLAGS="$CFLAGS -fbacktrace -ffree-line-length-none -fno-var-tracking-assignments -fbounds-check -finit-derived -frounding-math -fstack-protector-all -fcheck=all,no-array-temps -finit-real=snan -ffpe-trap=invalid,zero,overflow -fprofile-arcs"
else
  export CFLAGS="-O3"
  export FCFLAGS="$CFLAGS -DNDEBUG -ffree-line-length-none -frounding-math -fbounds-check -ffpe-trap=invalid,zero,overflow -fstack-protector-all -fno-var-tracking-assignments"
fi
export CXXFLAGS="$CFLAGS -std=c++17"
# General flags
export LDFLAGS="-L$mac_ports/lib"
export CPPFLAGS="-I$mac_ports/include"

# Configure
if [[ "$_arg_skip_config" == "no" ]]; then
  pushd "$_arg_root_path" && autoreconf -i && popd

  "$_arg_root_path"/configure --prefix="$_arg_prefix" \
    CC="$mac_ports/bin/mpicc-openmpi-gcc12" \
    FC="$mac_ports/bin/mpif90-openmpi-gcc12" \
    CXX="$mac_ports/bin/mpicxx-openmpi-gcc12" \
    --enable-mpi \
    --enable-openmp \
    --with-libxc-prefix="$mac_ports/libexec/libxc5" \
    --with-gsl-prefix="$mac_ports" \
    --with-fftw-prefix="$mac_ports" \
    --with-cgal="$mac_ports/lib/cmake/CGAL" \
    --with-netcdf-prefix="$mac_ports/lib/cmake/netCDF" \
    --with-netcdf-include="$mac_ports/include" \
    --with-blas="$mac_ports/lib/lapack/libblas.dylib" \
    --with-lapack="$mac_ports/lib/lapack/liblapack.dylib" \
    --with-blacs="$mac_ports/lib/libscalapack.dylib" \
    --with-scalapack="$mac_ports/lib/libscalapack.dylib"
fi

make -j $n_proc && make -j $n_proc install
