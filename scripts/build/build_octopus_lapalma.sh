 #!/bin/bash
export PATH=/gpfs/apps/MPICH2/mx/default/64/bin:$PATH
export LD_LIBRARY_PATH=/gpfs/apps/MPICH2/mx/default/64/lib:/gpfs/apps/MPICH2/slurm/64/lib

export LD_LIBRARY_PATH=/gpfs/apps/GCC/4.6.1/lib64/:$HOME/local/openblas/4.6.1/lib:$LD_LIBRARY_PATH
export CC="/gpfs/apps/MPICH2/mx/default/64/bin/mpicc -cc=/gpfs/apps/GCC/4.6.1/bin/gcc"
export FC="/gpfs/apps/MPICH2/mx/default/64/bin/mpif90 -fc=/gpfs/apps/GCC/4.6.1/bin/gfortran"

export CFLAGS="-m64 -O2 -I $PWD/external_libs/isf/wrappers "
export FCFLAGS=$CFLAGS

../configure \
--with-blas=$HOME/local/openblas/4.6.1/lib/libopenblas.so.0 \
--with-lapack=/gpfs/apps/LAPACK/lib64/liblapack.a \
--disable-gdlib \
--with-gsl-prefix=$HOME/local/gsl/gnu \
--with-fft-lib=$HOME/local/fftw/3.3.4/lib/libfftw3.a \
--with-libxc-prefix=$HOME/local/libxc/gnu/4.6.1/2.2 \
--prefix=/gpfs/projects/ehu31/software/octopus/gnu/4.6.1 \
--enable-mpi --disable-f90-forall
