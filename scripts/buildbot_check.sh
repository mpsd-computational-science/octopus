#! /bin/bash
# this script checks if buildbot master is marked as active in api
#
# requires: curl, jq

color_red="\033[31;49;1m"
color_green="\033[32;49;1m"
color_reset="\033[0m"

API_endpoint="https://octopus-code.org/buildbot/api/v2/masters"

if ! [[ -x "$(command -v jq)" ]]; then
    printf "%bERROR: jq could not be found!%b\n" "${color_red}" "${color_reset}"
    exit 1
fi

buildbot_master_info=$(curl -s -S "$API_endpoint")
buildbot_master_active=$(echo "$buildbot_master_info" | jq '.masters[].active')

if [[ "$buildbot_master_active" == "false" ]]; then
    printf "%bERROR: Buildbot master is not active!%b\nPlease restart this job after the octopus buildbot is activated.\n" "${color_red}" "${color_reset}"
    echo "Server response: $buildbot_master_info"
    exit 1
elif [[ "$buildbot_master_active" == "true" ]]; then
    printf "%bPASS: Buildbot master is active%b\n" "${color_green}" "${color_reset}"
else
    printf "%bERROR: Buildbot master status could not be verified!%b\n" "${color_red}" "${color_reset}"
    echo "buildbot_master_active: $buildbot_master_active"
    echo "Server response: $buildbot_master_info"
    exit 1
fi
